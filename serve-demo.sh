#!/usr/bin/env bash

haxe www-demo.hxml || exit 1

# https://github.com/http-party/http-server
http-server build/demo -p 0 -c-1 -o
