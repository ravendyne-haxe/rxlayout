// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxlayout.base.LayoutContainer;
import rxlayout.containers.HBox;
import rxlayout.containers.VBox;
import rxlayout.containers.StackingContainer;
import rxlayout.base.LayoutElement;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import js.Browser;

import rxlayout.types.IViewElement;
import rxlayout.types.LayoutBox;

using rxlayout.RxLayoutUtil;
using rxlayout.types.SizeBoxTools;

/////////////////////////////////////////////////////////////////////////////
//
// ROOT STUFF THAT FALLS UNDER GRAPHICS INTERFACE
//
/////////////////////////////////////////////////////////////////////////////

class HtmlViewNode {
    public static var debug : Bool = false;
    public var el(default,null) : js.html.DivElement;
    private var children(default,null) : Array<HtmlViewNode>;
    private var parent(default,null) : HtmlViewNode;
    public function new() {
        parent = null;
        el = null;
        children = [];
    }

    public function setParent( p : HtmlViewNode ) {
        parent.removeChild( this );
        p.registerChild( this );
        parent = p;
    }

    private function registerChild( child : HtmlViewNode ) {
        children.push( child );
        el.appendChild( child.el );
        child.parent = this;
    }

    private function removeChild( child : HtmlViewNode ) {
        children.remove( child );
        el.removeChild( child.el );
        child.parent = null;
    }

    public function destroy() {
        if( parent != null ) {
            parent.removeChild( this );
        }
        for( child in children ) {
            child.destroy();
        }
    }
}

class HtmlRootViewElement extends HtmlViewNode {
    public function new( div : js.html.DivElement ) {
        super();
        el = div;
    }
}

/////////////////////////////////////////////////////////////////////////////
//
// ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////

class HtmlViewElementBase extends HtmlViewNode implements IViewElement {

    public var layout(default,null) : ILayoutContainer;
    public var container(default,null) : LayoutContainer;

    public function new( parent_node : HtmlViewNode ) {
        super();
        parent = parent_node;
    }

    override private function registerChild( child : HtmlViewNode ) {
        super.registerChild( child );
        if( Std.isOfType( child, HtmlViewElementBase ) ) {
            var kid = cast( child, HtmlViewElementBase );
            layout.addChildren([ kid.layout ]);
        }
    }

    override private function removeChild( child : HtmlViewNode ) {
        super.removeChild( child );
        if( Std.isOfType( child, HtmlViewElementBase ) ) {
            var kid = cast( child, HtmlViewElementBase );
            layout.removeChildren([ kid.layout ]);
        }
    }

    public function setBox( box : LayoutBox ) : Void {
        if( box.x.isNumber() ) el.style.left = HtmlTools.toPx( box.x );
        if( box.y.isNumber() ) el.style.top = HtmlTools.toPx( box.y );
        if( box.width.isNumber() ) el.style.width = HtmlTools.toPx( box.width );
        if( box.height.isNumber() ) el.style.height = HtmlTools.toPx( box.height );
    }

    // public function setBorder( thickness : Float, colour : String = 'black', style : String = 'solid' ) : Void {
    //     if( ! thickness.isNumber() ) { el.style.border = null; return; }
    //     el.style.border = thickness + 'px' + ' ' + style + ' ' + colour;
    // }

    public function setBackground( colour : String ) : Void {
        if( colour == null ) { el.style.background = null; return; }
        el.style.backgroundColor = colour;
    }
}


class HtmlRectangle extends HtmlViewElementBase {

    public function new( parent_node : HtmlViewNode ) {
        super( parent_node );

        container = null;
        layout = new LayoutElement( this );

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';

        if( HtmlViewNode.debug ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
            el.style.boxSizing = 'border-box';
            el.style.border = '2px solid red';
        } else {
            el.style.border = '0px';
        }

        layout.setPositionAndSize( 0.px(), 0.px(), 0.px(), 0.px() );

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}

class HtmlCircle extends HtmlViewElementBase {

    var diameter : Float;

    public function new( parent_node : HtmlViewNode, diameter : Float = 32.0 ) {
        super( parent_node );

        container = null;
        layout = new LayoutElement( this );

        this.diameter = diameter;

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';
        el.style.borderRadius = '50%';

        if( HtmlViewNode.debug ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
            el.style.boxSizing = 'border-box';
            el.style.border = '2px solid red';
        } else {
            el.style.border = '0px';
        }

        layout.setPositionAndSize( 0.px(), 0.px(), diameter.px(), diameter.px() );

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }

    override public function setBox( box : LayoutBox ) : Void {
        if( box.x.isNumber() ) el.style.left = HtmlTools.toPx( box.x );
        if( box.y.isNumber() ) el.style.top = HtmlTools.toPx( box.y );
        var width = box.width.isNumber() ? box.width : diameter;
        var height = box.height.isNumber() ? box.height : diameter;
        diameter = Math.min( width, height );
        el.style.height = HtmlTools.toPx( diameter );
        el.style.width = HtmlTools.toPx( diameter );
    }
}


class HtmlEllipse extends HtmlViewElementBase {

    public function new( parent_node : HtmlViewNode ) {
        super( parent_node );

        container = null;
        layout = new LayoutElement( this );

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';
        el.style.borderRadius = '50%';

        if( HtmlViewNode.debug ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
            el.style.boxSizing = 'border-box';
            el.style.border = '2px solid red';
        } else {
            el.style.border = '0px';
        }

        layout.setPositionAndSize( 0.px(), 0.px(), 64.px(), 32.px() );

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
//
// CONTAINERS
//
/////////////////////////////////////////////////////////////////////////////

class HtmlContainerBase extends HtmlViewElementBase {

    public function new( parent_node : HtmlViewNode ) {
        super( parent_node );

        container = null;
        layout = container;

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';
        el.style.top = '0px';
        el.style.left = '0px';
        el.style.height = '0px';
        el.style.width = '0px';

        if( HtmlViewNode.debug ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
            el.style.boxSizing = 'border-box';
            el.style.border = '2px solid red';
        } else {
            el.style.border = '0px';
        }
    }
}


class HtmlContainer extends HtmlContainerBase {

    public function new( parent_node : HtmlViewNode ) {
        super( parent_node );

        container = new Container( this );
        layout = container;

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}


class HtmlStackingContainer extends HtmlContainerBase {

    public function new( parent_node : HtmlViewNode ) {
        super( parent_node );

        container = new StackingContainer( this );
        layout = container;

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}


class HtmlVBox extends HtmlContainerBase {

    public function new( parent_node : HtmlViewNode, gap : Float = 0.0 ) {
        super( parent_node );

        container = new VBox( gap, this );
        layout = container;

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}


class HtmlHBox extends HtmlContainerBase {

    public function new( parent_node : HtmlViewNode, gap : Float = 0.0 ) {
        super( parent_node );

        container = new HBox( gap, this );
        layout = container;

        if( parent_node != null ) {
            parent_node.registerChild( this );
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
//
// UTILS
//
/////////////////////////////////////////////////////////////////////////////

final class HtmlTools {
    private function new() {}

    public static function toPx( value : Float ) : String {
        if( ! value.isNumber() ) return '0px';

        return '${Std.int(value)}px';
    }
}
