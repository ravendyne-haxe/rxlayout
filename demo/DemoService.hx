// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxlayout.types.LayoutOptions;
import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import HtmlViewElement;

using rxlayout.RxLayoutUtil;
using rxlayout.types.SizeBoxTools;

@:keep
@:expose
class DemoService {

    public static function clear( container : js.html.DivElement ) {
        var c = container;
        while( c.firstChild != null ) {
            c.removeChild( c.firstChild );
        }
    }

    public static function initial( container : js.html.DivElement ) {

        var root = new HtmlRootViewElement( container );

        var b2 = new HtmlContainer( root );
        b2.setBackground( 'orange' );
        // b2.setBox({ x: 75., y: 200., width: 200., height: 100. });
        b2.layout.setPositionAndSize( 75.px(), 200.px(), 200.px(), 100.px() );

        var b3 = new HtmlRectangle( b2 );
        b3.setBackground( 'maroon' );
        b3.layout.setPositionAndSize( 10.px(), 10.px(), 64.px(), 32.px() );

        b2.layout.doLayout( LayoutBox.UNDEFINED );

        var b4 = new HtmlCircle( root, 48 );
        b4.setBackground( 'fuchsia' );
        b4.layout.setPosition( 64.px(), 32.px() );
        // b4.layout.setPositionAndSize( 64.px(), 32.px(), 64.px(), 32.px() );

        var b5 = new HtmlEllipse( root );
        b5.setBackground( 'darkgreen' );
        // b5.layout.setPosition( 256.px(), 32.px() );
        b5.layout.setPositionAndSize( 256.px(), 32.px(), 128.px(), 64.px() );
    }

    public static function demo_hbox_vbox( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlRootViewElement( container );

        hbox_demo( root, 32 + 0 * 272, 32 + 0 * 272, Start, None );
        hbox_demo( root, 32 + 1 * 272, 32 + 0 * 272, Center, None );
        hbox_demo( root, 32 + 2 * 272, 32 + 0 * 272, End, None );

        vbox_demo( root, 32 + 0 * 272, 32 + 1 * 272, None, Start );
        vbox_demo( root, 32 + 1 * 272, 32 + 1 * 272, None, Center );
        vbox_demo( root, 32 + 2 * 272, 32 + 1 * 272, None, End );
    }

    static function hbox_demo( root : HtmlViewNode, xpos : Float, ypos : Float, halign : AxisAlignment, valign : AxisAlignment ) {


        var b : HtmlVBox;
        var p : HtmlHBox;
        var c1 : HtmlRectangle;
        var c2 : HtmlRectangle;
        var c3 : HtmlRectangle;

        b = new HtmlVBox( root, 16 );
        b.container.margins = 16;
        b.setBackground( 'gray' );
        b.layout.setPosition( xpos.px(), ypos.px() );
        b.layout.setSize( 256.px(), SizeValue.UNDEFINED );

        //
        // START
        //
        p = new HtmlHBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? Start : halign;
        p.container.verticalAlignment = valign == None ? Start : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 32.px(), 16.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 32.px(), 48.px() );


        //
        // CENTER
        //
        p = new HtmlHBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? Center : halign;
        p.container.verticalAlignment = valign == None ? Center : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 32.px(), 16.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 32.px(), 48.px() );


        //
        // END
        //
        p = new HtmlHBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? End : halign;
        p.container.verticalAlignment = valign == None ? End : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 32.px(), 16.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 32.px(), 48.px() );


        b.layout.doLayout( LayoutBox.UNDEFINED );
    }

    static function vbox_demo( root : HtmlViewNode, xpos : Float, ypos : Float, halign : AxisAlignment, valign : AxisAlignment ) {


        var b : HtmlHBox;
        var p : HtmlVBox;
        var c1 : HtmlRectangle;
        var c2 : HtmlRectangle;
        var c3 : HtmlRectangle;

        b = new HtmlHBox( root, 16 );
        b.container.margins = 16;
        b.setBackground( 'gray' );
        b.layout.setPosition( xpos.px(), ypos.px() );
        b.layout.setSize( SizeValue.UNDEFINED, 256.px() );

        //
        // START
        //
        p = new HtmlVBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? Start : halign;
        p.container.verticalAlignment = valign == None ? Start : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 64.px(), 100.percent() );

        c1.layout.setSize( 16.px(), 32.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 48.px(), 32.px() );


        //
        // CENTER
        //
        p = new HtmlVBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? Center : halign;
        p.container.verticalAlignment = valign == None ? Center : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 64.px(), 100.percent() );

        c1.layout.setSize( 16.px(), 32.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 48.px(), 32.px() );


        //
        // END
        //
        p = new HtmlVBox( b, 8 );
        p.container.horizontalAlignment = halign == None ? End : halign;
        p.container.verticalAlignment = valign == None ? End : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 64.px(), 100.percent() );

        c1.layout.setSize( 16.px(), 32.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 48.px(), 32.px() );


        b.layout.doLayout( LayoutBox.UNDEFINED );
    }

    public static function demo_stacking( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlRootViewElement( container );

        stacking_demo( root, 32 + 0 * 272, 32 + 0 * 272, Start, None );
        stacking_demo( root, 32 + 1 * 272, 32 + 0 * 272, Center, None );
        stacking_demo( root, 32 + 2 * 272, 32 + 0 * 272, End, None );
    }

    static function stacking_demo( root : HtmlViewNode, xpos : Float, ypos : Float, halign : AxisAlignment, valign : AxisAlignment ) {


        var b : HtmlVBox;
        var p : HtmlStackingContainer;
        var c1 : HtmlRectangle;
        var c2 : HtmlRectangle;
        var c3 : HtmlRectangle;

        b = new HtmlVBox( root, 16 );
        b.container.margins = 16;
        b.setBackground( 'gray' );
        b.layout.setPosition( xpos.px(), ypos.px() );
        b.layout.setSize( 256.px(), SizeValue.UNDEFINED );

        //
        // START
        //
        p = new HtmlStackingContainer( b );
        p.container.horizontalAlignment = halign == None ? Start : halign;
        p.container.verticalAlignment = valign == None ? Start : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 48.px(), 48.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 16.px(), 16.px() );


        //
        // CENTER
        //
        p = new HtmlStackingContainer( b );
        p.container.horizontalAlignment = halign == None ? Center : halign;
        p.container.verticalAlignment = valign == None ? Center : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 48.px(), 48.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 16.px(), 16.px() );


        //
        // END
        //
        p = new HtmlStackingContainer( b );
        p.container.horizontalAlignment = halign == None ? End : halign;
        p.container.verticalAlignment = valign == None ? End : valign;
        p.setBackground( '#e1e2e1' );

        c1 = new HtmlRectangle( p );
        c1.setBackground( '#29434e' );
        c2 = new HtmlRectangle( p );
        c2.setBackground( '#546e7a' );
        c3 = new HtmlRectangle( p );
        c3.setBackground( '#819ca9' );

        p.layout.setSize( 100.percent(), 64.px() );

        c1.layout.setSize( 48.px(), 48.px() );
        c2.layout.setSize( 32.px(), 32.px() );
        c3.layout.setSize( 16.px(), 16.px() );


        b.layout.doLayout( LayoutBox.UNDEFINED );
    }
}
