#!/usr/bin/env bash

haxe www-tests.hxml || exit 1

# https://github.com/http-party/http-server
http-server build/tests -p 0 -c-1 -o
