// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import js.Browser;
import rxlayout.types.LayoutBox;
import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;
import containers.ContainerLayoutTestCaseBuilder;
import containers.StackingContainerLayoutTestCaseBuilder;
import containers.VBoxLayoutTestCaseBuilder;
import containers.HBoxLayoutTestCaseBuilder;
import containers.MiscLayoutsTestCaseBuilder;

using rxlayout.RxLayoutUtil;

// https://iamkate.com/data/12-bit-rainbow/
final class IAmKatePalette implements Palette {
    public function new(){}

    public var _0 = '#817';
    public var _1 = '#a35';
    public var _2 = '#c66';
    public var _3 = '#e94';
    public var _4 = '#ed0';
    public var _5 = '#9d5';
    public var _6 = '#4d8';
    public var _7 = '#2cb';
    public var _8 = '#0bc';
    public var _9 = '#09c';
    public var _a = '#36b';
    public var _b = '#639';
}
final class IAmKatePaletteGrayscale implements Palette {
    public function new(){}

    public var _0 = '#404040';
    public var _1 = '#5c5c5c';
    public var _2 = '#848484';
    public var _3 = '#a9a9a9';
    public var _4 = '#c9c9c9';
    public var _5 = '#b9b9b9';
    public var _6 = '#a6a6a6';
    public var _7 = '#979797';
    public var _8 = '#858585';
    public var _9 = '#717171';
    public var _a = '#606060';
    public var _b = '#4e4e4e';
}


@:keep
@:expose
class LayoutTestCaseService {

    static var palette : PaletteSet = {
        main: new IAmKatePalette(),
        grayscale: new IAmKatePaletteGrayscale(),
    }

    public static function clear( container : js.html.DivElement ) {
        var c = container;
        while( c.firstChild != null ) {
            c.removeChild( c.firstChild );
        }
    }

    static function addLabel( el : IViewElement, text : String ) : HtmlLabel {
        var b1 = cast( el, HtmlViewElement );
        return new HtmlLabel( b1.el, text );
    }

    public static function initial( container : js.html.DivElement ) {

        var factory = new HtmlViewElementBuilderFactory();
        var builder = new HtmlViewElementBuilder();

        var root = new HtmlViewElement( container );
        var b1 = factory.create()
        .setParent( root )
        .setColour( palette.main._5 )
        .setLabel( 'box-1' )
        .build();

        b1.setBox({ x: 75., y: 150., width: 200., height: 100. });


        var tcb = new MiscLayoutsTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.legend2( root, [
            { label: 'item', colour: palette.main._9 },
        ], palette.grayscale._4 );
        // tcb.sample( root, palette );
    }

    public static function test_case_container_e( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new ContainerLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseContainerWithElements( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_container_c( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new ContainerLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseContainerWithContainers( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_stacking_container_e( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new StackingContainerLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseStackingContainerFixedSizeWithElements( root, palette );
        tcb.caseStackingContainerNonFixedSizeWithElements( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_stacking_container_c( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new StackingContainerLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseStackingContainerFixedSizeWithContainers( root, palette );
        tcb.caseStackingContainerNonFixedSizeWithContainers( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_vbox_e( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new VBoxLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseVBoxFixedSizeWithElements( root, palette );
        tcb.caseVBoxNonFixedSizeWithElements( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_vbox_c( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new VBoxLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseVBoxFixedSizeWithContainers( root, palette );
        tcb.caseVBoxNonFixedSizeWithContainers( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_hbox_e( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new HBoxLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseHBoxFixedSizeWithElements( root, palette );
        tcb.caseHBoxNonFixedSizeWithElements( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_hbox_c( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new HBoxLayoutTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseHBoxFixedSizeWithContainers( root, palette );
        tcb.caseHBoxNonFixedSizeWithContainers( root, palette );
        tcb.legend( root, palette );
    }

    public static function test_case_misc_cases( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new MiscLayoutsTestCaseBuilder( new HtmlViewElementBuilderFactory() );
        tcb.caseMiscCases( root, palette );

        // theme = {
        //     palette: paletteSet.main,
        //     grayscale: paletteSet.grayscale,
        //     base: palette._5,
        //     parent: grayscale._7,
        //     legend: grayscale._4,
        //     kid_1: palette._b,
        //     kid_2: palette._a,
        //     kid_3: palette._9,
        //     kid_4: palette._8,
        //     kid_5: palette._2,
        // }

        tcb.legend2( root, [
            { label: 'parent - UND', colour: palette.grayscale._7 },
            { label: 'kid - UND', colour: palette.main._b },
            { label: 'kid - PX', colour: palette.main._a },
        ], palette.grayscale._4 );
    }

    public static function test_case_scratchpad( container : js.html.DivElement ) {

        clear( container );

        var root = new HtmlViewElement( container );
        var tcb = new LayoutTestCaseBuilderBase( new HtmlViewElementBuilderFactory() );
        tcb.scratchpad( root, palette );

        // theme = {
        //     palette: paletteSet.main,
        //     grayscale: paletteSet.grayscale,
        //     base: palette._5,
        //     parent: grayscale._7,
        //     legend: grayscale._4,
        //     kid_1: palette._b,
        //     kid_2: palette._a,
        //     kid_3: palette._9,
        //     kid_4: palette._8,
        //     kid_5: palette._2,
        // }

        tcb.legend2( root, [
            { label: 'parent - UND', colour: palette.grayscale._7 },
            { label: 'kid - AUTO', colour: palette.main._b },
            { label: 'kid - AUTO', colour: palette.main._a },
            { label: 'btn - UND', colour: palette.main._9 },
            { label: 'hbx - %', colour: palette.main._8 },
            { label: 'lbl - PX', colour: 'cyan' },
        ], palette.grayscale._4 );
    }
}

class HtmlViewElementBuilderFactory implements IViewElementBuilderFactory {
    public function new() {}

    public function create() : IViewElementBuilder {
        return new HtmlViewElementBuilder();
    }
}
class HtmlViewElementBuilder implements IViewElementBuilder {
    public static var defaultBackground : String = 'gray';
    // public static var defaultBorder : Float = 0.0;

    private var parent : IViewElement;
    private var label : String;
    private var colour : String;
    private var id : String;
    private var labelPosition : LabelPosition;
    public function new() {
        parent = null;
        label = null;
        id = null;
        labelPosition = Top;
        colour = defaultBackground;
    }
    public function setParent( parent : IViewElement ) : IViewElementBuilder {
        this.parent = parent;
        return this;
    }
    public function setColour( value : String ) : IViewElementBuilder {
        this.colour = value;
        return this;
    }
    public function setId( value : String ) : IViewElementBuilder {
        this.id = value;
        return this;
    }
    public function setLabel( text : String, where : LabelPosition = Top ) : IViewElementBuilder {
        this.label = text;
        this.labelPosition = where;
        return this;
    }
    public function build() : IViewElement {

        var pel : js.html.DivElement = null;
        if( parent != null ) { pel = (cast( parent, HtmlViewElement )).el; }

        var elem = new HtmlViewElement( pel );
        // elem.setBorder( border );
        elem.setBackground( colour );

        if( label != null ) {
            elem.addLabel( label, labelPosition );
        }

        if( id != null ) {
            elem.setAttribute( "id", id );
        }

        return elem;
    }
}

class HtmlElementBase {
    // public static var debug : Bool = false;
    public static var debug : Bool = true;
    public var el(default,null) : js.html.DivElement;
    public function new() {}
    public function getBox() : LayoutBox {
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
        var dom_rect = el.getBoundingClientRect();
        // var cs = Browser.window.getComputedStyle(el);
        var parent_x = 0.0;
        var parent_y = 0.0;
        if( el.parentElement != null ) {
            var parent_rect = el.parentElement.getBoundingClientRect();
            parent_x = parent_rect.x;
            parent_y = parent_rect.y;
        }
        var lb : LayoutBox = {
            x: dom_rect.left - parent_x,
            y: dom_rect.top - parent_y,
            width: dom_rect.width,
            height: dom_rect.height,
            // width: el.clientWidth * 1.,
            // height: el.clientHeight * 1.,
        }
        // trace( 'lb = ${lb}' );
        return lb;
    }
}

class HtmlViewElement extends HtmlElementBase implements IViewElement {
    private var label : HtmlLabel;
    private var labelPosition : LabelPosition;

    public function new( parent : js.html.DivElement ) {
        super();

        label = null;

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';
        el.style.top = '0px';
        el.style.left = '0px';
        el.style.height = '0px';
        el.style.width = '0px';

        if( HtmlElementBase.debug ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
            el.style.boxSizing = 'border-box';
            // el.style.border = '2px solid red';
            el.style.border = '2px dashed red';
            // el.style.border = '2px dotted red';
        } else {
            el.style.border = '0px';
        }

        if( parent != null ) {
            parent.appendChild( el );
        }
    }
    public function setBox( box : LayoutBox ) : Void {
        if( box.x.isNumber() ) el.style.left = box.x + 'px';
        if( box.y.isNumber() ) el.style.top = box.y + 'px';
        if( box.width.isNumber() ) el.style.width = box.width + 'px';
        if( box.height.isNumber() ) el.style.height = box.height + 'px';
        // once we update layout, setBox() will be called, label positions will change in HTML
        // and so will how text is rendered, and so will text size, and
        // so will label size, so we need to update label position AFTER this element's size is updated.
        if( label != null ) label.updatePosition( labelPosition );
    }

    // public function setBorder( thickness : Float, colour : String = 'black', style : String = 'solid' ) : Void {
    //     if( ! thickness.isNumber() ) { el.style.border = null; return; }
    //     el.style.border = thickness + 'px' + ' ' + style + ' ' + colour;
    // }

    public function addLabel( text : String, where : LabelPosition = Top ) : Void {
        label = new HtmlLabel( this.el, text, where );
        label.text_el.style.padding = '2px';
        labelPosition = where;
        label.updatePosition( labelPosition );
    }

    public function setBackground( colour : String ) : Void {
        if( colour == null ) { el.style.background = null; return; }
        el.style.backgroundColor = colour;
    }

    public function setAttribute( name : String, value : String ) : Void {
        if( value == null ) { el.removeAttribute( name ); return; }
        el.setAttribute( name, value );
    }
}

class HtmlLabel extends HtmlElementBase {
    var parent : js.html.DivElement;
    var text : String;
    public var text_el(default,null) : js.html.DivElement;
    public function new( parent : js.html.DivElement, text : String, where : LabelPosition = Top ) {
        super();
        this.parent = parent;

        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
        el = Browser.document.createDivElement();
        el.style.position = 'absolute';
        el.style.top = '0px';
        el.style.left = '0px';
        // el.style.height = '0px';
        // el.style.width = '0px';
        el.style.backgroundColor = 'black';

        // el.style.boxSizing = 'border-box';
        el.style.border = '0px';
        el.style.padding = '1px';

        if( parent != null ) {
            parent.appendChild( el );
        }

        text_el = Browser.document.createDivElement();
        // text_el.style.position = 'absolute';
        // text_el.style.top = '0px';
        // text_el.style.left = '0px';
        // text_el.style.height = '100%';
        // text_el.style.width = '100%';
        text_el.style.backgroundColor = 'white';
        // text_el.style.boxSizing = 'border-box';
        text_el.style.border = '0px';
        el.appendChild( text_el );

        setText( text );

        updatePosition( where );
    }
    public function updatePosition( where : LabelPosition ) {
        // move the box label just out of the box
        var bb = getBox();
        // trace( '"${text}": ${bb}' );
        switch( where ) {
            case Top:
                el.style.top = '-' + bb.height + 'px';
                el.style.left = '0px';
            case Right:
                el.style.top = '-' + bb.height + 'px';
                el.style.left = '0px';
            case Bottom:
                el.style.top = '-' + bb.height + 'px';
                el.style.left = '0px';
            case Left:
                el.style.top = '0px';
                el.style.left = '-' + bb.width + 'px';
        }
    }
    public function setText( text : String ) : Void {
        this.text = text;
        text_el.textContent = text;
    }
}
