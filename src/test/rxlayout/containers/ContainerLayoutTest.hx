// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.base.LayoutContainer;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import containers.TestViewElementBuilder.TestViewElementBuilderFactory;

using rxlayout.types.SizeBoxTools;


class ContainerLayoutTest extends Test {

    function testContainerLayoutWithElements() {

        var tcb = new ContainerLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseContainerWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 160.0, width: 280.0, height: 420.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 60.0, y: 60.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 60.0, width: 140.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 60.0, y: 300.0, width: 50.0, height: 140.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 300.0, width: 140.0, height: 140.0 }, k.getBox());
    }

    function testContainerLayoutWithContainers() {

        var tcb = new ContainerLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseContainerWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 160.0, width: 280.0, height: 420.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 60.0, y: 60.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 60.0, width: 140.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 60.0, y: 300.0, width: 50.0, height: 140.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 300.0, y: 300.0, width: 140.0, height: 140.0 }, k.getBox());
    }

    function testContainerDoLayoutMultipleTimesWithElements() {
        var pve = new TestViewElement();
        var p = new Container( pve );

        var cve = new TestViewElement();
        var c = new LayoutElement( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
        // Assert.same( { x: 16.0, y: 16.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }

    function testContainerDoLayoutMultipleTimesWithContainers() {
        var pve = new TestViewElement();
        var p = new Container( pve );

        var cve = new TestViewElement();
        var c = new LayoutContainer( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
        // Assert.same( { x: 16.0, y: 16.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }
}
