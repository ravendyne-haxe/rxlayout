// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.types.ILayoutContainer;
import utest.Assert;
import utest.Test;

import containers.TestViewElementBuilder.TestViewElementBuilderFactory;


class MiscLayoutsTest extends Test {

    function testVBoxLayoutWithElements() {

        var tcb = new MiscLayoutsTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseMiscCases( new TestViewElement() );

        var k : TestViewElement;

        Assert.equals( 1, TestUtils.iteratorLength( c.children ) );
        c = c.children.next();
        k = cast( c.view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 190.0, height: 115.0 }, k.getBox());

        Assert.equals( 1, TestUtils.iteratorLength( c.children ) );
        c = c.children.next();
        k = cast( c.view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 150.0, height: 75.0 }, k.getBox());
    }
}
