// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.base.LayoutContainer;
import rxlayout.containers.VBox;
import rxlayout.types.ILayoutContainer;
import containers.TestViewElementBuilder.TestViewElementBuilderFactory;

using rxlayout.types.SizeBoxTools;


class VBoxLayoutTest extends Test {

    function testVBoxLayoutWithElements() {

        var tcb = new VBoxLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseVBoxFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 75.0, height: 150.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 180.0, width: 50.0, height: 150.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 350.0, width: 75.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 420.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 490.0, width: 300.0, height: 120.0 }, k.getBox());


        c = tcb.caseVBoxNonFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 25.0, height: 0.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 30.0, width: 100.0, height: 0.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 50.0, width: 25.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 120.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 190.0, width: 100.0, height: 0.0 }, k.getBox());
    }

    function testVBoxLayoutWithContainers() {

        var tcb = new VBoxLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseVBoxFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 75.0, height: 150.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 180.0, width: 50.0, height: 150.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 350.0, width: 75.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 420.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 490.0, width: 300.0, height: 120.0 }, k.getBox());


        c = tcb.caseVBoxNonFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 25.0, height: 0.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 30.0, width: 100.0, height: 0.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 50.0, width: 25.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 120.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 190.0, width: 100.0, height: 0.0 }, k.getBox());
    }

    function testContainerDoLayoutMultipleTimesWithElements() {
        var pve = new TestViewElement();
        var p = new VBox( pve );

        var cve = new TestViewElement();
        var c = new LayoutElement( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }

    function testContainerDoLayoutMultipleTimesWithContainers() {
        var pve = new TestViewElement();
        var p = new VBox( pve );

        var cve = new TestViewElement();
        var c = new LayoutContainer( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }
}
