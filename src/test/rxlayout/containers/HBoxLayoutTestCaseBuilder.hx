// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.containers.HBox;
import rxlayout.base.LayoutContainer;
import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;

using rxlayout.types.SizeBoxTools;


class HBoxLayoutTestCaseBuilder extends LayoutTestCaseBuilderBase {

    public function caseHBoxFixedSizeWithElements( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new HBox( 20 );
        cc.margins = 10;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'HBox - fixed size E' ), root_c );
        c.container.setPositionAndSize( 75.px(), 50.px(), 620.px(), 320.px() );

        //
        // KIDS
        //

        // kids positions are ignored in vbox container,
        // only their sizes matter

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 40.px(), 40.px(), SizeValue.AUTO, SizeValue.AUTO );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseHBoxNonFixedSizeWithElements( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new HBox( 20 );
        cc.margins = 10;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'HBox - undefined size E' ), root_c );
        c.container.setPositionAndSize( 75.px(), (50 + 350).px(), SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //

        // kids positions are ignored in vbox container,
        // only their sizes matter

        // in non-sized vbox, kids that have height in non-px (%, base)
        // will end up with their height being 0px.

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 100.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 40.px(), 40.px(), SizeValue.AUTO, SizeValue.AUTO );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseHBoxFixedSizeWithContainers( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new HBox( 20 );
        cc.margins = 10;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'HBox - fixed size C' ), root_c );
        c.container.setPositionAndSize( 75.px(), 50.px(), 620.px(), 320.px() );

        //
        // KIDS
        //

        // kids positions are ignored in vbox container,
        // only their sizes matter

        var k1 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k5 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 40.px(), 40.px(), SizeValue.AUTO, SizeValue.AUTO );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseHBoxNonFixedSizeWithContainers( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new HBox( 20 );
        cc.margins = 10;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'HBox - undefined size C' ), root_c );
        c.container.setPositionAndSize( 75.px(), (50 + 350).px(), SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //

        // kids positions are ignored in vbox container,
        // only their sizes matter

        // in non-sized vbox, kids that have height in non-px (%, base)
        // will end up with their height being 0px.

        var k1 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 100.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k5 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 40.px(), 40.px(), SizeValue.AUTO, SizeValue.AUTO );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

}
