// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.base.LayoutContainer;
import rxlayout.containers.HBox;
import rxlayout.types.ILayoutContainer;
import containers.TestViewElementBuilder.TestViewElementBuilderFactory;

using rxlayout.types.SizeBoxTools;


class HBoxLayoutTest extends Test {

    function testHBoxLayoutWithElements() {

        var tcb = new HBoxLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseHBoxFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 150.0, height: 75.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 180.0, y: 10.0, width: 50.0, height: 75.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 250.0, y: 10.0, width: 150.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 420.0, y: 10.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 540.0, y: 10.0, width: 70.0, height: 300.0 }, k.getBox());


        c = tcb.caseHBoxNonFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 0.0, height: 12.5 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 30.0, y: 10.0, width: 100.0, height: 12.5 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 150.0, y: 10.0, width: 0.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 170.0, y: 10.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 290.0, y: 10.0, width: 0.0, height: 50.0 }, k.getBox());
    }

    function testHBoxLayoutWithContainers() {

        var tcb = new HBoxLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseHBoxFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 150.0, height: 75.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 180.0, y: 10.0, width: 50.0, height: 75.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 250.0, y: 10.0, width: 150.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 420.0, y: 10.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 540.0, y: 10.0, width: 70.0, height: 300.0 }, k.getBox());


        c = tcb.caseHBoxNonFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 10.0, y: 10.0, width: 0.0, height: 12.5 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 30.0, y: 10.0, width: 100.0, height: 12.5 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 150.0, y: 10.0, width: 0.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 170.0, y: 10.0, width: 100.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 290.0, y: 10.0, width: 0.0, height: 50.0 }, k.getBox());
    }

    function testContainerDoLayoutMultipleTimesWithElements() {
        var pve = new TestViewElement();
        var p = new HBox( pve );

        var cve = new TestViewElement();
        var c = new LayoutElement( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }

    function testContainerDoLayoutMultipleTimesWithContainers() {
        var pve = new TestViewElement();
        var p = new HBox( pve );

        var cve = new TestViewElement();
        var c = new LayoutContainer( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }
}
