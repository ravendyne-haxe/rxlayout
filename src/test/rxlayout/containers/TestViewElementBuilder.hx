// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;


class TestViewElementBuilderFactory implements IViewElementBuilderFactory {
    public function new() {}

    public function create() : IViewElementBuilder {
        return new TestViewElementBuilder();
    }
}
class TestViewElementBuilder implements IViewElementBuilder {
    public function new() {
    }
    public function setParent( parent : IViewElement ) : IViewElementBuilder {
        return this;
    }
    public function setColour( value : String ) : IViewElementBuilder {
        return this;
    }
    public function setId( value : String ) : IViewElementBuilder {
        return this;
    }
    public function setLabel( text : String, where : LabelPosition = Top ) : IViewElementBuilder {
        return this;
    }
    public function build() : IViewElement {
        return new TestViewElement();
    }
}
