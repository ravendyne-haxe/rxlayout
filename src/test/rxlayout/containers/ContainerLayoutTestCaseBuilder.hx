// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.base.LayoutContainer;
import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;

using rxlayout.types.SizeBoxTools;


class ContainerLayoutTestCaseBuilder extends LayoutTestCaseBuilderBase {

    public function caseContainerWithElements( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //
        var cc = new Container();
        cc.margins = 20;
        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'Container - fixed size E' ), root_c );

        c.container.setPositionAndSize( 75.px(), 50.px(), 600.px(), 600.px() );


        //
        // KIDS
        //

        // var margins_outline = new LayoutElement( factory.create().setParent( c_ve ).setColour( 'none' ).build() );
        // margins_outline.setPositionAndSize( 0.px(), 0.px(), 100.percent(), 100.percent() );

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 25.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k1.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k2.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k3.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k4.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseContainerWithContainers( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new Container();
        cc.margins = 20;
        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'Container - fixed size C' ), root_c );

        c.container.setPositionAndSize( 75.px(), 50.px(), 600.px(), 600.px() );

        //
        // KIDS
        //
        var k5 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 25.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k1 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_1 ), c );
        k1.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        var k2 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_2 ), c );
        k2.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k3 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_3 ), c );
        k3.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k4 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_4 ), c );
        k4.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );


        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }
}
