// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.types.SizeValue;
import rxlayout.containers.StackingContainer;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;

using rxlayout.types.SizeBoxTools;


class StackingContainerLayoutTestCaseBuilder extends LayoutTestCaseBuilderBase {

    public function caseStackingContainerFixedSizeWithElements( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new StackingContainer();
        cc.margins = 20;
        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'StackingContainer - fixed size E' ), root_c );

        c.container.setPositionAndSize( 75.px(), 50.px(), 600.px(), 300.px() );

        //
        // KIDS
        //

        // kids positions are ignored in stacking container,
        // only their sizes matter

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 50.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseStackingContainerNonFixedSizeWithElements( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new StackingContainer();
        cc.margins = 20;
        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'StackingContainer - undefined size E' ), root_c );

        c.container.setPositionAndSize( 75.px(), (50 + 350).px(), SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 50.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 200.px(), 100.px() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 100.px(), 25.percent() );

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseStackingContainerFixedSizeWithContainers( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new StackingContainer();
        cc.margins = 20;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'StackingContainer - fixed size C' ), root_c );

        c.container.setPositionAndSize( 75.px(), 50.px(), 600.px(), 300.px() );

        //
        // KIDS
        //

        // kids positions are ignored in stacking container,
        // only their sizes matter

        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 50.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 50.px(), 25.percent() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 100.px(), 50.px() );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

    public function caseStackingContainerNonFixedSizeWithContainers( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new StackingContainer();
        cc.margins = 20;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'StackingContainer - undefined size C' ), root_c );

        c.container.setPositionAndSize( 75.px(), (50 + 350).px(), SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //

        // kids positions are ignored in stacking container,
        // only their sizes matter
        var k5 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_5 ), c );
        k5.container.setPositionAndSize( 50.percent(), 50.percent(), SizeValue.AUTO, SizeValue.AUTO );

        var k4 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_1 ), c );
        k4.container.setPositionAndSize( 40.px(), 40.px(), 200.px(), 100.px() );

        var k3 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_2 ), c );
        k3.container.setPositionAndSize( 50.percent(), 40.px(), 25.percent(), 50.px() );

        var k2 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_3 ), c );
        k2.container.setPositionAndSize( 40.px(), 50.percent(), 100.px(), 25.percent() );

        var k1 = new TestContainerNode( new LayoutElement(null), factory.create().setColour( theme.kid_4 ), c );
        k1.container.setPositionAndSize( 50.percent(), 50.percent(), 25.percent(), 25.percent() );

        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }

}
