// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import rxlayout.base.LayoutContainer;
import rxlayout.types.SizeValue;
import rxlayout.containers.StackingContainer;
import rxlayout.types.LayoutBox;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;
import LayoutTestCaseBuilderBase;

using rxlayout.types.SizeBoxTools;


class MiscLayoutsTestCaseBuilder extends LayoutTestCaseBuilderBase {

    public function caseMiscCases( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var cc = new StackingContainer();
        cc.margins = 20;
        cc.horizontalAlignment = Center;

        var c = new TestContainerNode( cc, factory.create().setColour( theme.parent ).setLabel( 'Multi-level kids' ), root_c );
        c.container.setPositionAndSize( (75).px(), 150.px(), SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //

        var k1c = new StackingContainer();
        k1c.margins = 20;
        var k1 = new TestContainerNode( k1c, factory.create().setColour( theme.kid_1 ), c );
        // k1.container.setSize( 100.percent(), 100.percent() );
        // k1.container.setSize( SizeValue.AUTO, SizeValue.AUTO );
        k1.container.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );


        var k2 = new TestContainerNode( new LayoutContainer(), factory.create().setColour( theme.kid_2 ), k1 );
        k2.container.setSize( 150.px(), 75.px() );

        // c.container.doLayout( {x:75.,y:150.,width:300.,height:300.} );
        c.container.doLayout( LayoutBox.UNDEFINED );

        return c.container;
    }
}
