// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package containers;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.base.LayoutContainer;
import rxlayout.containers.StackingContainer;
import rxlayout.types.ILayoutContainer;
import containers.TestViewElementBuilder.TestViewElementBuilderFactory;

using rxlayout.types.SizeBoxTools;


class StackingContainerLayoutTest extends Test {

    function testStackingContainerLayoutWithElements() {

        var tcb = new StackingContainerLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseStackingContainerFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 560.0, height: 260.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 140.0, height: 65.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 65.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 140.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 100.0, height: 50.0 }, k.getBox());


        c = tcb.caseStackingContainerNonFixedSizeWithElements( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 200.0, height: 100.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 200.0, height: 100.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 100.0, height: 25.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 25.0 }, k.getBox());
    }

    function testStackingContainerLayoutWithContainers() {

        var tcb = new StackingContainerLayoutTestCaseBuilder( new TestViewElementBuilderFactory() );
        var c : ILayoutContainer;


        c = tcb.caseStackingContainerFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 560.0, height: 260.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 140.0, height: 65.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 65.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 140.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 100.0, height: 50.0 }, k.getBox());


        c = tcb.caseStackingContainerNonFixedSizeWithContainers( new TestViewElement() );

        Assert.equals( 5, TestUtils.iteratorLength( c.children ) );

        var kids : Iterator<ILayoutContainer> = c.children;
        var k : TestViewElement;
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 200.0, height: 100.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 200.0, height: 100.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 50.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 100.0, height: 25.0 }, k.getBox());
        k = cast( kids.next().view_element, TestViewElement );
        Assert.same( { x: 20.0, y: 20.0, width: 50.0, height: 25.0 }, k.getBox());
    }

    function testContainerDoLayoutMultipleTimesWithElements() {
        var pve = new TestViewElement();
        var p = new StackingContainer( pve );

        var cve = new TestViewElement();
        var c = new LayoutElement( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }

    function testContainerDoLayoutMultipleTimesWithContainers() {
        var pve = new TestViewElement();
        var p = new StackingContainer( pve );

        var cve = new TestViewElement();
        var c = new LayoutContainer( cve );

        p.addChildren([
            c,
        ]);

        p.setPosition( 0.px(), 0.px() );
        p.setSize( 100.px(), 100.px() );
        p.margins = 8;

        c.setPosition( 0.px(), 0.px() );
        c.setSize( SizeValue.AUTO, SizeValue.AUTO );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );

        p.doLayout( LayoutBox.UNDEFINED );

        Assert.same( { x: 8.0, y: 8.0, width: 84.0, height: 84.0 }, cve.getBox() );
    }
}
