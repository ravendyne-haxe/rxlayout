// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxlayout.types.LayoutBox;
import rxlayout.types.IViewElement;

class TestViewElement implements IViewElement {
    var box : LayoutBox;
    public function new( ?b : LayoutBox ) { b != null ? box = b.clone() : box = LayoutBox.ZERO; }
    public function setBox( b : LayoutBox ) : Void { box = b.clone(); }
    public function getBox() : LayoutBox { return box.clone(); }
}
