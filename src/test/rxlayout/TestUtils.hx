// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

final class TestUtils {
    private function new() {}
    public static function iteratorLength<T>( it : Iterator<T> ) : Int {
        var length : Int = 0;
        while( it.hasNext() ) {
            length++;
            it.next();
        }
        return length;
    }
}
