// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

import rxlayout.base.LayoutContainer;

/**
 * This class exposes `layoutParameters` private property
 * of the `LayoutContainer` so that we can set up different
 * test cases.
 */
class TestLayoutContainer extends LayoutContainer {
    public var layoutOptions(get,null) : LayoutParameters;
    function get_layoutOptions() { return layoutParameters; }
}
