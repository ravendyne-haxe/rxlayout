// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package base;

import utest.Assert;
import utest.Test;

import rxlayout.base.LayoutContainer;
import rxlayout.types.SizeValue;
import rxlayout.types.ILayoutContainer;
import rxlayout.base.LayoutElement;
import rxlayout.types.SizeBox;
import rxlayout.types.LayoutBox;

using rxlayout.types.SizeBoxTools;


class LayoutElementTest extends Test {

    function testDefaults() {

        var c : ILayoutContainer;
        var ve : TestViewElement;

        ve = new TestViewElement();
        Assert.same( LayoutBox.ZERO, ve.getBox() );

        c = new LayoutElement( ve );

        Assert.same( LayoutBox.UNDEFINED, c.layout_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );

        Assert.isNull( c.parent );
        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
    }

    function testSetSizeAndPosition() {

        var c : ILayoutContainer;

        c = new LayoutElement( new TestViewElement() );

        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );

        //
        // PX
        //
        c.setPosition( 2.px(), 4.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.px(), 16.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: 8.px(), height: 16.px() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 8, height: 16 }, c.packed_size );

        c.setPositionAndSize( 32.px(), 64.px(), 128.px(), 256.px() );
        Assert.same( { x: 32.px(), y: 64.px(), width: 128.px(), height: 256.px() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 128, height: 256 }, c.packed_size );

        c.setPositionAndSizeBox({ x: 2.px(), y: 4.px(), width: 6.px(), height: 8.px() });
        Assert.same( { x: 2.px(), y: 4.px(), width: 6.px(), height: 8.px() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 6, height: 8 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: SizeValue.UNDEFINED, y: SizeValue.UNDEFINED, width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );

        //
        // %
        //
        c.setPosition( 2.percent(), 4.percent() );
        Assert.same( { x: 2.percent(), y: 4.percent(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.percent(), 16.percent() );
        Assert.same( { x: 2.percent(), y: 4.percent(), width: 8.percent(), height: 16.percent() }, c.assigned_box );
        Assert.same( { x: 0, y: 0,width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( 32.percent(), 64.percent(), 128.percent(), 256.percent() );
        Assert.same( { x: 32.percent(), y: 64.percent(), width: 128.percent(), height: 256.percent() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox({ x: 2.percent(), y: 4.percent(), width: 6.percent(), height: 8.percent() });
        Assert.same( { x: 2.percent(), y: 4.percent(), width: 6.percent(), height: 8.percent() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        //
        // base
        //
        c.setPosition( 2.base(), 4.base() );
        Assert.same( { x: 2.base(), y: 4.base(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.base(), 16.base() );
        Assert.same( { x: 2.base(), y: 4.base(), width: 8.base(), height: 16.base() }, c.assigned_box );
        Assert.same( { x: 0, y: 0,width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( 32.base(), 64.base(), 128.base(), 256.base() );
        Assert.same( { x: 32.base(), y: 64.base(), width: 128.base(), height: 256.base() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox({ x: 2.base(), y: 4.base(), width: 6.base(), height: 8.base() });
        Assert.same( { x: 2.base(), y: 4.base(), width: 6.base(), height: 8.base() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        //
        // mix
        //
        c.setPosition( 2.px(), 4.percent() );
        Assert.same( { x: 2.px(), y: 4.percent(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.px(), 16.percent() );
        Assert.same( { x: 2.px(), y: 4.percent(), width: 8.px(), height: 16.percent() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 8, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPosition( 2.px(), 4.base() );
        Assert.same( { x: 2.px(), y: 4.base(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.px(), 16.base() );
        Assert.same( { x: 2.px(), y: 4.base(), width: 8.px(), height: 16.base() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 8, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPosition( 2.percent(), 4.base() );
        Assert.same( { x: 2.percent(), y: 4.base(), width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setSize( 8.percent(), 16.base() );
        Assert.same( { x: 2.percent(), y: 4.base(), width: 8.percent(), height: 16.base() }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( 32.px(), 64.percent(), 128.base(), SizeValue.UNDEFINED );
        Assert.same( { x: 32.px(), y: 64.percent(), width: 128.base(), height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox({ x: 2.px(), y: 4.percent(), width: 6.base(), height: SizeValue.UNDEFINED });
        Assert.same( { x: 2.px(), y: 4.percent(), width: 6.base(), height: SizeValue.UNDEFINED }, c.assigned_box );
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );
    }

    function testAddRemoveChildren() {

        var c : ILayoutContainer;

        c = new LayoutElement( new TestViewElement() );

        var c1 = new LayoutContainer();
        var c2 = new LayoutContainer();

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        c.addChildren([
            c1,
            c2,
        ]);
        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );

        c.removeChildren([
            c1,
        ]);
        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
    }

    function testUpdatePackedSize() {

        var c : ILayoutContainer;

        c = new LayoutElement( new TestViewElement() );

        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );

        c.setPosition( 2.px(), 4.px() );
        c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );
    }

    function testGetPreferredBox() {

        var c : ILayoutContainer;

        c = new LayoutElement( new TestViewElement() );

        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPosition( 2.px(), 4.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 2.px(), height: 4.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        Assert.same( { x: 0.px(), y: 64.px(), width: 128.percent(), height: 256.base() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );
    }
}
