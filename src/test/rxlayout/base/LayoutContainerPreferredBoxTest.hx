// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package base;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.SizeBox;
import rxlayout.types.ILayoutContainer;

using rxlayout.types.SizeBoxTools;


class LayoutContainerPreferredBoxNoChildrenTest extends Test {

    function testGetPreferredBoxLayoutNone() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        c.layoutOptions.mainAxis.type = None;
        c.layoutOptions.crossAxis.type = None;
        c.margins = 0.0;

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPosition( 2.px(), 4.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 2.px(), height: 4.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        Assert.same( { x: 0.px(), y: 64.px(), width: 128.percent(), height: 256.base() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );
    }

    function testGetPreferredBoxLayoutStacked() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        c.layoutOptions.mainAxis.type = Stacked;
        c.layoutOptions.crossAxis.type = Stacked;
        c.margins = 0.0;

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPosition( 2.px(), 4.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 2.px(), height: 4.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        Assert.same( { x: 0.px(), y: 64.px(), width: 128.percent(), height: 256.base() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );
    }

    function testGetPreferredBoxLayoutAligned() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        c.layoutOptions.mainAxis.type = Aligned;
        c.layoutOptions.mainAxis.align = Start;
        c.layoutOptions.crossAxis.type = Aligned;
        c.layoutOptions.crossAxis.align = Start;
        c.margins = 0.0;

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPosition( 2.px(), 4.px() );
        Assert.same( { x: 2.px(), y: 4.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 2.px(), height: 4.px() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        Assert.same( { x: 0.px(), y: 64.px(), width: 128.percent(), height: 256.base() }, c.getPreferredBox() );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );
    }
}


class LayoutContainerPreferredBoxWithChildrenTest extends Test {

    function testGetPreferredBoxLayoutNone() {

        var c : TestLayoutContainer;
        var c1 : TestLayoutContainer;
        var c2 : TestLayoutContainer;

        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = None;
        c.layoutOptions.crossAxis.type = None;
        c.margins = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );
        Assert.same( { x: 0.px(), y: 0.px(), width: 18.px(), height: 28.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 4.px(), 6.px(), 12.px(), 28.px() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 18.px(), height: 34.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 4.px(), 6.px(), 12.px(), 28.px() );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 100.px(), 200.px(), 300.px(), 400.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 4.px(), 6.px(), 12.px(), 28.px() );
        Assert.same( { x: 100.px(), y: 200.px(), width: 300.px(), height: 400.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( SizeValue.UNDEFINED, 64.px(), 128.percent(), 256.base() );
        c2.setPositionAndSize( 32.px(), SizeValue.UNDEFINED, 64.percent(), 128.base() );
        Assert.same( { x: 0.px(), y: 0.px(), width: 32.px(), height: 64.px() }, c.getPreferredBox() );
    }

    function testGetPreferredBoxLayoutStacked() {

        var c : TestLayoutContainer;
        var c1 : ILayoutContainer;
        var c2 : ILayoutContainer;


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Stacked;
        c.layoutOptions.crossAxis.type = Stacked;
        c.margins = 0.0;
        c.gaps = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 0.px(), y: 0.px(), width: 16.px(), height: 24.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 24.px(), height: 48.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.gaps = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 28.px(), height: 52.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.gaps = 4.0;
        c.margins = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 36.px(), height: 60.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );
    }

    function testGetPreferredBoxLayoutAligned() {

        var c : TestLayoutContainer;
        var c1 : ILayoutContainer;
        var c2 : ILayoutContainer;


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Aligned;
        c.layoutOptions.mainAxis.align = Start;
        c.layoutOptions.crossAxis.type = Aligned;
        c.layoutOptions.crossAxis.align = Start;
        c.margins = 0.0;
        c.gaps = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 0.px(), y: 0.px(), width: 0.px(), height: 0.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 0.px(), y: 0.px(), width: 16.px(), height: 24.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 16.px(), height: 24.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.margins = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 24.px(), height: 32.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.margins = 0.0;
        c.layoutOptions.mainAxis.align = Center;
        c.layoutOptions.crossAxis.align = Center;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 16.px(), height: 24.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );


        c.layoutOptions.mainAxis.align = End;
        c.layoutOptions.crossAxis.align = End;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 0.px(), y: 0.px(), width: 16.px(), height: 24.px() }, c.getPreferredBox() );


        c.setPositionAndSize( 1.px(), 2.px(), 3.px(), 4.px() );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, c.getPreferredBox() );
    }
}
