// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package base;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;
import rxlayout.types.SizeBox;
import rxlayout.types.LayoutBox;
import rxlayout.types.ILayoutContainer;

using rxlayout.types.SizeBoxTools;


class LayoutContainerUpdatePackedSizeNoChildrenTest extends Test {

    function testLayoutNone() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );


        c.layoutOptions.mainAxis.type = None;
        c.layoutOptions.crossAxis.type = None;
        c.margins = 0.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = None;
        c.layoutOptions.crossAxis.type = None;
        c.margins = 6.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );

        c.setSize( 36.px(), 48.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 36, height: 48 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );
    }

    function testLayoutStacked() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );


        c.layoutOptions.mainAxis.type = Stacked;
        c.layoutOptions.crossAxis.type = Stacked;
        c.margins = 0.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Stacked;
        c.layoutOptions.crossAxis.type = Stacked;
        c.margins = 6.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );

        c.setSize( 36.px(), 48.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 36, height: 48 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );
    }

    function testLayoutAligned() {

        var c : TestLayoutContainer;

        c = new TestLayoutContainer();

        Assert.equals( 0, TestUtils.iteratorLength( c.children ) );
        Assert.same( SizeBox.UNDEFINED, c.assigned_box );
        Assert.same( LayoutBox.ZERO, c.packed_size );


        c.layoutOptions.mainAxis.type = Aligned;
        c.layoutOptions.mainAxis.align = Start;
        c.layoutOptions.crossAxis.type = Aligned;
        c.layoutOptions.crossAxis.align = Start;
        c.margins = 0.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Aligned;
        c.layoutOptions.mainAxis.align = Start;
        c.layoutOptions.crossAxis.type = Aligned;
        c.layoutOptions.crossAxis.align = Start;
        c.margins = 6.0;

        c.setPosition( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSizeBox( SizeBox.UNDEFINED );

        c.setSize( 2.px(), 4.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 2, height: 4 }, c.packed_size );

        c.setSize( 36.px(), 48.px() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 36, height: 48 }, c.packed_size );


        c.setPositionAndSize( 256.base(), 256.base(), 256.base(), 256.base() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( 128.percent(), 128.percent(), 128.percent(), 128.percent() );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );

        c.setPositionAndSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 12, height: 12 }, c.packed_size );
    }
}


class LayoutContainerUpdatePackedSizeWithChildrenTest extends Test {

    function testLayoutNone() {

        var c : TestLayoutContainer;
        var c1 : ILayoutContainer;
        var c2 : ILayoutContainer;


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = None;
        c.layoutOptions.crossAxis.type = None;
        c.margins = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 18, height: 28 }, c.packed_size );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 18, height: 32 }, c.packed_size );
    }

    function testLayoutStacked() {

        var c : TestLayoutContainer;
        var c1 : ILayoutContainer;
        var c2 : ILayoutContainer;


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Stacked;
        c.layoutOptions.crossAxis.type = Stacked;
        c.margins = 0.0;
        c.gaps = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 16, height: 24 }, c.packed_size );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 24, height: 48 }, c.packed_size );


        c.gaps = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 28, height: 52 }, c.packed_size );


        c.gaps = 4.0;
        c.margins = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 36, height: 60 }, c.packed_size );
    }

    function testAligned() {

        var c : TestLayoutContainer;
        var c1 : ILayoutContainer;
        var c2 : ILayoutContainer;


        c = new TestLayoutContainer();
        c.layoutOptions.mainAxis.type = Aligned;
        c.layoutOptions.mainAxis.align = Start;
        c.layoutOptions.crossAxis.type = Aligned;
        c.layoutOptions.crossAxis.align = Start;
        c.margins = 0.0;
        c.gaps = 0.0;
        c1 = new TestLayoutContainer();
        c2 = new TestLayoutContainer();

        c.addChildren([ c1, c2 ]);


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 0, height: 0 }, c.packed_size );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSizeBox( SizeBox.UNDEFINED );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 16, height: 24 }, c.packed_size );


        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 16, height: 24 }, c.packed_size );


        c.margins = 4.0;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 24, height: 32 }, c.packed_size );


        c.margins = 0.0;
        c.layoutOptions.mainAxis.align = Center;
        c.layoutOptions.crossAxis.align = Center;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 16, height: 24 }, c.packed_size );


        c.layoutOptions.mainAxis.align = End;
        c.layoutOptions.crossAxis.align = End;
        c.setPositionAndSizeBox( SizeBox.UNDEFINED );
        c1.setPositionAndSize( 2.px(), 4.px(), 16.px(), 24.px() );
        c2.setPositionAndSize( 2.px(), 8.px(), 8.px(), 24.px() );

        // c.updatePackedSize();
        Assert.same( { x: 0, y: 0, width: 16, height: 24 }, c.packed_size );
    }
}
