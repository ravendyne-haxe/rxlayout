// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxlayout.containers.HBox;
import rxlayout.containers.VBox;
import rxlayout.types.SizeValue;
import rxlayout.containers.StackingContainer;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutElement;
import rxlayout.containers.Container;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;

using rxlayout.types.SizeBoxTools;

interface Palette {

    var _0 : String;
    var _1 : String;
    var _2 : String;
    var _3 : String;
    var _4 : String;
    var _5 : String;
    var _6 : String;
    var _7 : String;
    var _8 : String;
    var _9 : String;
    var _a : String;
    var _b : String;
}

enum LabelPosition {
    Top;
    Right;
    Bottom;
    Left;
}

// interface ITestViewElement extends IViewElement {
// }

interface IViewElementBuilderFactory {
    function create() : IViewElementBuilder;
}

interface IViewElementBuilder {
    function setParent( parent : IViewElement ) : IViewElementBuilder;
    function setColour( value : String ) : IViewElementBuilder;
    function setId( value : String ) : IViewElementBuilder;
    function setLabel( text : String, where : LabelPosition = Top ) : IViewElementBuilder;
    function build() : IViewElement;
}

final class DummyPalette implements Palette {
    public function new(){}

    public var _0 = 'fuchsia';
    public var _1 = 'fuchsia';
    public var _2 = 'fuchsia';
    public var _3 = 'fuchsia';
    public var _4 = 'fuchsia';
    public var _5 = 'fuchsia';
    public var _6 = 'fuchsia';
    public var _7 = 'fuchsia';
    public var _8 = 'fuchsia';
    public var _9 = 'fuchsia';
    public var _a = 'fuchsia';
    public var _b = 'fuchsia';
}

typedef PaletteSet = {
    var main: Palette;
    var grayscale: Palette;
}

typedef Theme = {
    var palette: Palette;
    var grayscale: Palette;
    var base: String;
    var parent: String;
    var kid_1: String;
    var kid_2: String;
    var kid_3: String;
    var kid_4: String;
    var kid_5: String;
    var legend: String;
}

typedef LegendItem = {
    var label: String;
    var colour: String;
}

class TestContainerNode {
    public var container : ILayoutContainer;
    public var parent : TestContainerNode;
    public var viewelement : IViewElement;

    public function new( container : ILayoutContainer, ?ve : IViewElement, ?vebuilder : IViewElementBuilder, ?parent : TestContainerNode ) {
        this.container = container;

        if( ve != null ) {
            viewelement = ve;
            parent = null;
        } else if( parent != null ) {
            this.parent = parent;
            parent.container.addChildren([ container ]);
            if( vebuilder != null ) {
                viewelement = vebuilder.setParent( parent.viewelement ).build();
            }
        }

        container.view_element = viewelement;
    }
}

class LayoutTestCaseBuilderBase {
    var factory : IViewElementBuilderFactory;

    function getTheme( ?paletteSet : PaletteSet ) : Theme {
        paletteSet = paletteSet != null ? paletteSet : {
            main: new DummyPalette(),
            grayscale: new DummyPalette(),
        };

        var palette = paletteSet.main;
        var grayscale = paletteSet.grayscale;

        return {
            palette: paletteSet.main,
            grayscale: paletteSet.grayscale,
            base: palette._5,
            parent: grayscale._7,
            // parent: 'gray',
            legend: grayscale._4,
            kid_1: palette._b,
            kid_2: palette._a,
            kid_3: palette._9,
            kid_4: palette._8,
            kid_5: palette._2,
        }
    }

    public function new( factory : IViewElementBuilderFactory ) {
        this.factory = factory;
    }

    public function legend( root : IViewElement, ?palette : PaletteSet ) : Void {
        var theme = getTheme( palette );

        //
        // CONTAINER
        //
        var r_box_ve = factory.create()
        .setParent( root )
        .setLabel( 'Legend' )
        .setColour( theme.legend )
        .build();
        var r_box_c = new VBox( 20, r_box_ve );
        r_box_c.margins = 20;
        r_box_c.setPositionAndSize( 750.px(), 50.px(), 100.px(), SizeValue.UNDEFINED );


        //
        // KIDS
        //
        var k1 = new LayoutElement( factory.create().setLabel( 'px x px', Left ).setColour( theme.kid_1 ).setParent( r_box_ve ).build() );
        k1.setSize( 100.percent(), 50.px() );

        var k2 = new LayoutElement( factory.create().setLabel( '% x px', Left ).setColour( theme.kid_2 ).setParent( r_box_ve ).build() );
        k2.setSize( 100.percent(), 50.px() );

        var k3 = new LayoutElement( factory.create().setLabel( 'px x %', Left ).setColour( theme.kid_3 ).setParent( r_box_ve ).build() );
        k3.setSize( 100.percent(), 50.px() );

        var k4 = new LayoutElement( factory.create().setLabel( '% x %', Left ).setColour( theme.kid_4 ).setParent( r_box_ve ).build() );
        k4.setSize( 100.percent(), 50.px() );

        var k5 = new LayoutElement( factory.create().setLabel( 'auto x auto', Left ).setColour( theme.kid_5 ).setParent( r_box_ve ).build() );
        k5.setSize( 100.percent(), 50.px() );

        // var k6 = new LayoutElement( factory.create().setLabel( 'auto x px', Left ).setColour( theme.kid_5 ).setParent( r_box_ve ).build() );
        // k6.setSize( 100.percent(), 50.px() );

        r_box_c.addChildren([
            k1,
            k2,
            k3,
            k4,
            k5,
            // k6,
        ]);

        r_box_c.doLayout( LayoutBox.UNDEFINED );
    }

    public function legend2( root : IViewElement, items : Array<LegendItem>, legend_colour : String ) : Void {

        var r_box_ve = factory.create()
        .setParent( root )
        .setLabel( 'Legend' )
        .setColour( legend_colour )
        .build();
        var r_box_c = new VBox( 20, r_box_ve );
        r_box_c.margins = 20;
        r_box_c.setPositionAndSize( 750.px(), 50.px(), 100.px(), SizeValue.UNDEFINED );


        var children : Array<ILayoutContainer> = [];
        for( item in items ) {
            var kid = new LayoutElement( factory.create().setLabel( item.label, Left ).setColour( item.colour ).setParent( r_box_ve ).build() );
            kid.setSize( 100.percent(), 50.px() );
            children.push( kid );
        }

        r_box_c.addChildren( children );

        r_box_c.doLayout( LayoutBox.UNDEFINED );
    }

    public function sample( root : IViewElement, ?palette : PaletteSet ) : Void {
        var theme = getTheme( palette );

        var root_c = new TestContainerNode( new Container(), root );

        var sc = new StackingContainer();
        sc.margins = 20;
        var r_box_o_c = new TestContainerNode( sc, factory.create().setLabel( 'Legend' ).setColour( theme.legend ), root_c );
        r_box_o_c.container.setPositionAndSize( 750.px(), 50.px(), 100.px(), SizeValue.UNDEFINED );

        var r_box_c = new TestContainerNode( new Container(), factory.create().setLabel( 'thingie' ).setColour( theme.kid_3 ), r_box_o_c );
        r_box_c.container.setPosition( 0.px(), 0.px() );
        // r_box_c.container.setSize( 100.percent(), SizeValue.UNDEFINED );
        r_box_c.container.setSize( 100.percent(), 50.px() );

        r_box_o_c.container.doLayout( LayoutBox.UNDEFINED );
    }

    public function scratchpad( root : IViewElement, ?palette : PaletteSet ) : ILayoutContainer {
        var theme = getTheme( palette );
        var root_c = new TestContainerNode( new Container(), root );

        //
        // CONTAINER
        //

        var _ui_layer_c = new Container();
        _ui_layer_c.margins = 8;
        _ui_layer_c.horizontalAlignment = Start;
        var _ui_layer = new TestContainerNode( _ui_layer_c, factory.create().setColour( theme.parent ).setLabel( 'Multi-level kids' ).setId('_ui_layer'), root_c );
        // _ui_layer.container.horizontalAlignment = Center;
        // _ui_layer.container.horizontalAlignment = End;
        _ui_layer.container.setPosition( (75).px(), 150.px() );
        _ui_layer.container.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );

        //
        // KIDS
        //
        var drawing_ui_c = new VBox();
        drawing_ui_c.margins = 8;
        var drawing_ui = new TestContainerNode( drawing_ui_c, factory.create().setColour( theme.kid_1 ).setId('drawing_ui'), _ui_layer );
        drawing_ui.container.setSize( SizeValue.AUTO, SizeValue.AUTO );


        var top_bar_c = new HBox();
        top_bar_c.margins = 8;
        var top_bar = new TestContainerNode( top_bar_c, factory.create().setColour( theme.kid_2 ).setId('top_bar'), drawing_ui );
        top_bar.container.setSize( SizeValue.AUTO, 72.px() );
        // top_bar.container.setSize( 160.px(), 72.px() );

        var button_new_c = new StackingContainer();
        button_new_c.verticalAlignment = Center;
        button_new_c.margins = 4;
        // this collapses button to 0 width
        var button_new = new TestContainerNode( button_new_c, factory.create().setColour( theme.kid_3 ).setId('button_new'), top_bar );
        button_new.container.setSize( SizeValue.UNDEFINED, 48.px() );
        // button_new.container.setSize( 100.px(), 48.px() );
        // button_new.container.setSize( 100.px(), SizeValue.AUTO );

        var _button_content_c = new HBox();
        _button_content_c.verticalAlignment = Center;
        _button_content_c.margins = 10;
        _button_content_c.gaps = 5;
        var _button_content = new TestContainerNode( _button_content_c, factory.create().setColour( theme.kid_4 ).setId('_button_content'), button_new );
        _button_content.container.setPosition( 0.px(), 0.px() );
        // _button_content.container.setSize( 100.percent(), 100.percent() );
        _button_content.container.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // _button_content.container.setSize( SizeValue.AUTO, SizeValue.AUTO );

        var label = new TestContainerNode( new LayoutElement(null), factory.create().setColour( 'cyan' ).setId('label'), _button_content );
        label.container.setSize( 65.px(), 24.px() );


        _ui_layer.container.setSize( 250.px(), 250.px() );
        _ui_layer.container.doLayout( LayoutBox.UNDEFINED );

        return _ui_layer.container;
    }
}
