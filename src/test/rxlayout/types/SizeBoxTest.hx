// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package types;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeBox;
import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.types.Constants;

using rxlayout.types.SizeBoxTools;


class SizeBoxTest extends Test {

    function testDefaults() {

        var sb : SizeBox;

        Assert.isTrue( SizeBox.ZERO != SizeBox.ZERO );
        Assert.isTrue( SizeBox.UNDEFINED != SizeBox.UNDEFINED );

        sb = SizeBox.ZERO;
        Assert.same( 0.px(), sb.x );
        Assert.same( 0.px(), sb.y );
        Assert.same( 0.px(), sb.width );
        Assert.same( 0.px(), sb.height );

        sb = SizeBox.UNDEFINED;
        Assert.isTrue( sb.x.isUndefined() );
        Assert.isTrue( sb.y.isUndefined() );
        Assert.isTrue( sb.width.isUndefined() );
        Assert.isTrue( sb.height.isUndefined() );
    }

    function testFrom() {

        var sb : SizeBox;

        Assert.isTrue( SizeBox.ZERO != SizeBox.ZERO );
        Assert.isTrue( SizeBox.UNDEFINED != SizeBox.UNDEFINED );

        sb = SizeBox.fromValues( 1.px(), 1.percent(), 1.base(), SizeValue.UNDEFINED );
        Assert.same( 1.px(), sb.x );
        Assert.same( 1.percent(), sb.y );
        Assert.same( 1.base(), sb.width );
        Assert.same( SizeValue.UNDEFINED, sb.height );

        sb = SizeBox.fromValues( 1.px(), 1.percent(), 1.base(), SizeValue.AUTO );
        Assert.same( 1.px(), sb.x );
        Assert.same( 1.percent(), sb.y );
        Assert.same( 1.base(), sb.width );
        Assert.same( SizeValue.AUTO, sb.height );

        sb = SizeBox.fromSize( 3.0, 4.0 );
        Assert.same( SizeValue.UNDEFINED, sb.x );
        Assert.same( SizeValue.UNDEFINED, sb.y );
        Assert.same( 3.px(), sb.width );
        Assert.same( 4.px(), sb.height );

        sb = SizeBox.fromSize( Const.UNDEFINED_VALUE, 4.0 );
        Assert.same( SizeValue.UNDEFINED, sb.x );
        Assert.same( SizeValue.UNDEFINED, sb.y );
        Assert.same( SizeValue.UNDEFINED, sb.width );
        Assert.same( 4.px(), sb.height );

        sb = SizeBox.fromSize( 3.0, Const.UNDEFINED_VALUE );
        Assert.same( SizeValue.UNDEFINED, sb.x );
        Assert.same( SizeValue.UNDEFINED, sb.y );
        Assert.same( 3.px(), sb.width );
        Assert.same( SizeValue.UNDEFINED, sb.height );


        sb = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        var sb2 = sb.clone();
        Assert.isTrue( sb2 != sb );
        Assert.same( sb2, sb );


        sb = SizeBox.fromLayoutBox({ x: 1.0, y: 2.0, width: 3.0, height: 4.0 });
        Assert.same( 1.px(), sb.x );
        Assert.same( 2.px(), sb.y );
        Assert.same( 3.px(), sb.width );
        Assert.same( 4.px(), sb.height );

        sb = SizeBox.fromLayoutBox({ x: Const.UNDEFINED_VALUE, y: Const.UNDEFINED_VALUE, width: Const.UNDEFINED_VALUE, height: Const.UNDEFINED_VALUE });
        Assert.same( SizeValue.UNDEFINED, sb.x );
        Assert.same( SizeValue.UNDEFINED, sb.y );
        Assert.same( SizeValue.UNDEFINED, sb.width );
        Assert.same( SizeValue.UNDEFINED, sb.height );
    }

    function testAssign() {

        var lb1 : SizeBox;
        var lb2 : SizeBox;

        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );

        lb1.assignBox( lb2 );
        Assert.same( lb1, lb2 );


        //
        // assignBoxIfLeftIsUndefined
        //

        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( SizeValue.UNDEFINED, 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfLeftIsUndefined( lb2 );
        Assert.same( { x: 5.px(), y: 2.px(), width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), SizeValue.UNDEFINED, 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 6.px(), width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), SizeValue.UNDEFINED, 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), SizeValue.UNDEFINED );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 8.px() }, lb1 );


        //
        // assignSizeIfLeftIsUndefined
        //

        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( SizeValue.UNDEFINED, 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfLeftIsUndefined( lb2 );
        Assert.same( { x: SizeValue.UNDEFINED, y: 2.px(), width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), SizeValue.UNDEFINED, 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: SizeValue.UNDEFINED, width: 3.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), SizeValue.UNDEFINED, 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 4.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), SizeValue.UNDEFINED );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfLeftIsUndefined( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 8.px() }, lb1 );


        //
        // assignSizeIfRightIsSet
        //

        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( SizeValue.UNDEFINED, 6.px(), 7.px(), 8.px() );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), SizeValue.UNDEFINED, 7.px(), 8.px() );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), SizeValue.UNDEFINED, 8.px() );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 3.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), SizeValue.UNDEFINED );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 2.px(), width: 7.px(), height: 4.px() }, lb1 );


        //
        // assignBoxIfRightIsSet
        //

        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.px(), y: 6.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( SizeValue.UNDEFINED, 6.px(), 7.px(), 8.px() );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 1.px(), y: 6.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), SizeValue.UNDEFINED, 7.px(), 8.px() );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.px(), y: 2.px(), width: 7.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), SizeValue.UNDEFINED, 8.px() );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.px(), y: 6.px(), width: 3.px(), height: 8.px() }, lb1 );


        lb1 = SizeBox.fromValues( 1.px(), 2.px(), 3.px(), 4.px() );
        lb2 = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), SizeValue.UNDEFINED );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.px(), y: 6.px(), width: 7.px(), height: 4.px() }, lb1 );
    }
}
