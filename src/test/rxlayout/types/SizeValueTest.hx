// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package types;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeValue;

using rxlayout.types.SizeBoxTools;


class SizeValueTest extends Test {

    function testDefaults() {

        var sv : SizeValue;

        Assert.isTrue( SizeValue.ZERO != SizeValue.ZERO );
        Assert.isTrue( SizeValue.UNDEFINED != SizeValue.UNDEFINED );
        Assert.isTrue( SizeValue.AUTO != SizeValue.AUTO );

        sv = SizeValue.ZERO;
        Assert.same( 0.px(), sv );

        sv = SizeValue.UNDEFINED;
        Assert.isTrue( sv.isUndefined() );
        Assert.isFalse( sv.isAuto() );

        sv = SizeValue.AUTO;
        Assert.isFalse( sv.isUndefined() );
        Assert.isTrue( sv.isAuto() );

        sv = 0.px();
        Assert.isFalse( sv.isUndefined() );
        Assert.isFalse( sv.isAuto() );

        sv = 0.percent();
        Assert.isFalse( sv.isUndefined() );
        Assert.isFalse( sv.isAuto() );

        sv = 0.base();
        Assert.isFalse( sv.isUndefined() );
        Assert.isFalse( sv.isAuto() );
    }
}
