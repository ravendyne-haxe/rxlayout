// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package types;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeBox;
import rxlayout.types.SizeValue;
import rxlayout.types.LayoutBox;
import rxlayout.types.Constants;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;

class LayoutBoxTest extends Test {

    function testDefaults() {

        var lb : LayoutBox;

        Assert.isTrue( LayoutBox.ZERO != LayoutBox.ZERO );
        Assert.isTrue( LayoutBox.UNDEFINED != LayoutBox.UNDEFINED );

        Assert.same( {x: 0.0, y: 0.0, width: 0.0, height: 0.0}, LayoutBox.ZERO );

        lb = LayoutBox.UNDEFINED;
        Assert.isFalse( lb.x.isNumber() );
        Assert.isFalse( lb.y.isNumber() );
        Assert.isFalse( lb.width.isNumber() );
        Assert.isFalse( lb.height.isNumber() );
    }

    function testFromSizeBox() {

        // all PX
        Assert.same(
            { x: 1.0, y: 2.0, width: 3.0, height: 4.0 },
            LayoutBox.fromSizeBox({ x: 1.px(), y: 2.px(), width: 3.px(), height: 4.px() })
        );

        // all PERCENT
        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.percent(), y: 2.percent(), width: 3.percent(), height: 4.percent() })
        );

        // all BASE
        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.base(), y: 2.base(), width: 3.base(), height: 4.base() })
        );

        // all UNDEFINED
        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: SizeValue.UNDEFINED, y: SizeValue.UNDEFINED, width: SizeValue.UNDEFINED, height: SizeValue.UNDEFINED })
        );

        // all AUTO
        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: SizeValue.AUTO, y: SizeValue.AUTO, width: SizeValue.AUTO, height: SizeValue.AUTO })
        );

        // rotate with UNDEFINED
        Assert.same(
            { x: 1.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.px(), y: SizeValue.UNDEFINED, width: 1.base(), height: 1.percent() })
        );

        Assert.same(
            { x: 0.0, y: 1.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.percent(), y: 1.px(), width: SizeValue.UNDEFINED, height: 1.base() })
        );

        Assert.same(
            { x: 0.0, y: 0.0, width: 1.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.base(), y: 1.percent(), width: 1.px(), height: SizeValue.UNDEFINED })
        );

        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 1.0 },
            LayoutBox.fromSizeBox({ x: SizeValue.UNDEFINED, y: 1.base(), width: 1.percent(), height: 1.px() })
        );

        // rotate with AUTO
        Assert.same(
            { x: 1.0, y: 0.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.px(), y: SizeValue.AUTO, width: 1.base(), height: 1.percent() })
        );

        Assert.same(
            { x: 0.0, y: 1.0, width: 0.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.percent(), y: 1.px(), width: SizeValue.AUTO, height: 1.base() })
        );

        Assert.same(
            { x: 0.0, y: 0.0, width: 1.0, height: 0.0 },
            LayoutBox.fromSizeBox({ x: 1.base(), y: 1.percent(), width: 1.px(), height: SizeValue.AUTO })
        );

        Assert.same(
            { x: 0.0, y: 0.0, width: 0.0, height: 1.0 },
            LayoutBox.fromSizeBox({ x: SizeValue.AUTO, y: 1.base(), width: 1.percent(), height: 1.px() })
        );

    }

    function testFromXYZ() {

        Assert.same(
            { x: 1.0, y: 2.0, width: 3.0, height: 4.0 },
            LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 )
        );

        var lb = LayoutBox.fromSize( 3.0, 4.0 );
        Assert.isFalse( lb.x.isNumber() );
        Assert.isFalse( lb.y.isNumber() );
        Assert.equals( 3.0, lb.width );
        Assert.equals( 4.0, lb.height );

        var lb2 = lb.clone();
        Assert.isTrue( lb != lb2 );
        Assert.same( lb2, lb );
    }

    function testAssign() {

        var lb1 : LayoutBox;
        var lb2 : LayoutBox;

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );

        lb1.assignBox( lb2 );
        Assert.same( lb1, lb2 );


        //
        // assignBoxIfLeftNotSet
        //

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( Const.UNDEFINED_VALUE, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfLeftNotSet( lb2 );
        Assert.same( { x: 5.0, y: 2.0, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, Const.UNDEFINED_VALUE, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 6.0, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, Const.UNDEFINED_VALUE, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, Const.UNDEFINED_VALUE );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 8.0 }, lb1 );


        //
        // assignSizeIfLeftNotSet
        //

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( Const.UNDEFINED_VALUE, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfLeftNotSet( lb2 );
        Assert.same( { x: Const.UNDEFINED_VALUE, y: 2.0, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, Const.UNDEFINED_VALUE, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: Const.UNDEFINED_VALUE, width: 3.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, Const.UNDEFINED_VALUE, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 4.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, Const.UNDEFINED_VALUE );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfLeftNotSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 8.0 }, lb1 );


        //
        // assignSizeIfRightIsSet
        //

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( Const.UNDEFINED_VALUE, 6.0, 7.0, 8.0 );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, Const.UNDEFINED_VALUE, 7.0, 8.0 );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, Const.UNDEFINED_VALUE, 8.0 );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, Const.UNDEFINED_VALUE );
        lb1.assignSizeIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 4.0 }, lb1 );


        //
        // assignBoxIfRightIsSet
        //

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.0, y: 6.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( Const.UNDEFINED_VALUE, 6.0, 7.0, 8.0 );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 1.0, y: 6.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, Const.UNDEFINED_VALUE, 7.0, 8.0 );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.0, y: 2.0, width: 7.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, Const.UNDEFINED_VALUE, 8.0 );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.0, y: 6.0, width: 3.0, height: 8.0 }, lb1 );


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, Const.UNDEFINED_VALUE );
        lb1.assignBoxIfRightIsSet( lb2 );
        Assert.same( { x: 5.0, y: 6.0, width: 7.0, height: 4.0 }, lb1 );
    }

    function testAssignFromSizeBox() {

        var lb : LayoutBox;
        var sb : SizeBox;


        //
        // assignBoxFromSizeBox
        //

        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 5.0, y: 6.0, width: 7.0, height: 8.0 }, lb );

        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 4.0 }, lb );

        lb = LayoutBox.fromValues( Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE );
        sb = SizeBox.fromValues( SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: Const.UNDEFINED_VALUE, y: Const.UNDEFINED_VALUE, width: Const.UNDEFINED_VALUE, height: Const.UNDEFINED_VALUE }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( SizeValue.UNDEFINED, 6.px(), 7.px(), 8.px() );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 6.0, width: 7.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), SizeValue.UNDEFINED, 7.px(), 8.px() );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 5.0, y: 2.0, width: 7.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), SizeValue.UNDEFINED, 8.px() );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 5.0, y: 6.0, width: 3.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), SizeValue.UNDEFINED );
        lb.assignBoxFromSizeBox( sb );
        Assert.same( { x: 5.0, y: 6.0, width: 7.0, height: 4.0 }, lb );


        //
        // assignSizeFromSizeBox
        //

        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), 8.px() );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb );

        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 4.0 }, lb );

        lb = LayoutBox.fromValues( Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE );
        sb = SizeBox.fromValues( SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: Const.UNDEFINED_VALUE, y: Const.UNDEFINED_VALUE, width: Const.UNDEFINED_VALUE, height: Const.UNDEFINED_VALUE }, lb );

        lb = LayoutBox.fromValues( 1.0, 2.0, Const.UNDEFINED_VALUE, Const.UNDEFINED_VALUE );
        sb = SizeBox.fromValues( SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO, SizeValue.AUTO );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: Const.UNDEFINED_VALUE, height: Const.UNDEFINED_VALUE }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( SizeValue.UNDEFINED, 6.px(), 7.px(), 8.px() );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), SizeValue.UNDEFINED, 7.px(), 8.px() );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), SizeValue.UNDEFINED, 8.px() );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 3.0, height: 8.0 }, lb );


        lb = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        sb = SizeBox.fromValues( 5.px(), 6.px(), 7.px(), SizeValue.UNDEFINED );
        lb.assignSizeFromSizeBox( sb );
        Assert.same( { x: 1.0, y: 2.0, width: 7.0, height: 4.0 }, lb );
    }

    function testUtils() {

        var lb1 : LayoutBox;
        var lb2 : LayoutBox;

        lb1 = LayoutBox.fromValues( 1.0, 2.0, 3.0, 4.0 );
        lb2 = LayoutBox.fromValues( 5.0, 6.0, 7.0, 8.0 );

        lb1.expandTo( lb2 );

        Assert.same( { x: 1.0, y: 2.0, width: 12.0, height: 14.0 }, lb1 );

        // TODO add expandTo() cases for undefined values


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 12.0, 14.0 );

        lb1.clampToMaxSize( { x: 999.0, y: 999.0, width: 9.0, height: 16.0 } );

        Assert.same( { x: 1.0, y: 2.0, width: 9.0, height: 14.0 }, lb1 );

        // TODO add clampToMaxSize() cases for undefined values


        lb1 = LayoutBox.fromValues( 1.0, 2.0, 12.0, 14.0 );

        lb1.stretchToMinSize( { x: 999.0, y: 999.0, width: 9.0, height: 16.0 } );

        Assert.same( { x: 1.0, y: 2.0, width: 12.0, height: 16.0 }, lb1 );

        // TODO add stretchToMinSize() cases for undefined values
    }
}
