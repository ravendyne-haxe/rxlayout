// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

// import utest.Runner;
// import utest.ui.Report;
import utest.UTest;


class TestsMain {

    public static function main() {

        UTest.run([

            new types.LayoutBoxTest(),
            new types.SizeBoxTest(),
            new types.SizeValueTest(),

            new algorithm.LayoutDimensionTest(),
            new algorithm.LayoutDimensionSizeTest(),

            new algorithm.Algorithm_Stacked_WithMargins_Test(),
            new algorithm.Algorithm_Stacked_WithMargins_Test.Algorithm_Stacked_WithMargins_NoGap_Test(),
            new algorithm.Algorithm_Stacked_WithMargins_Test.Algorithm_Stacked_WithMargins_WithGap_Test(),
            new algorithm.Algorithm_Stacked_NoMargins_Test(),
            new algorithm.Algorithm_Stacked_NoMargins_Test.Algorithm_Stacked_NoMargins_NoGap_Test(),
            new algorithm.Algorithm_Stacked_NoMargins_Test.Algorithm_Stacked_NoMargins_WithGap_Test(),

            new algorithm.Algorithm_Aligned_WithMargins_Test(),
            new algorithm.Algorithm_Aligned_WithMargins_Test.Algorithm_Aligned_WithMargins_NoGap_Test(),
            new algorithm.Algorithm_Aligned_WithMargins_Test.Algorithm_Aligned_WithMargins_WithGap_Test(),
            new algorithm.Algorithm_Aligned_NoMargins_Test(),
            new algorithm.Algorithm_Aligned_NoMargins_Test.Algorithm_Aligned_NoMargins_NoGap_Test(),
            new algorithm.Algorithm_Aligned_NoMargins_Test.Algorithm_Aligned_NoMargins_WithGap_Test(),

            new algorithm.Algorithm_None_NoMargins_Test(),
            new algorithm.Algorithm_None_NoMargins_Test.Algorithm_None_NoMargins_NoGap_Test(),
            new algorithm.Algorithm_None_NoMargins_Test.Algorithm_None_NoMargins_WithGap_Test(),
            new algorithm.Algorithm_None_WithMargins_Test(),
            new algorithm.Algorithm_None_WithMargins_Test.Algorithm_None_WithMargins_NoGap_Test(),
            new algorithm.Algorithm_None_WithMargins_Test.Algorithm_None_WithMargins_WithGap_Test(),

            new algorithm.Layout_Algorithm_None_Test(),
            new algorithm.Layout_Algorithm_Aligned_Test(),
            new algorithm.Layout_Algorithm_Stacked_Test(),

            new base.LayoutElementTest(),
            new base.LayoutContainerTest(),
            new base.LayoutContainerUpdatePackedSizeTest.LayoutContainerUpdatePackedSizeNoChildrenTest(),
            new base.LayoutContainerUpdatePackedSizeTest.LayoutContainerUpdatePackedSizeWithChildrenTest(),
            new base.LayoutContainerPreferredBoxTest.LayoutContainerPreferredBoxNoChildrenTest(),
            new base.LayoutContainerPreferredBoxTest.LayoutContainerPreferredBoxWithChildrenTest(),

            new containers.StackingContainerLayoutTest(),
            new containers.ContainerLayoutTest(),
            new containers.VBoxLayoutTest(),
            new containers.HBoxLayoutTest(),
            new containers.MiscLayoutsTest(),
        ]);
    }
}
