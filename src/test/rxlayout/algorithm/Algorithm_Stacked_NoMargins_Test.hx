// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package algorithm;

import utest.Assert;
import utest.Test;

import rxlayout.algorithm.LayoutDimension;
import rxlayout.types.SizeValue;
import rxlayout.algorithm.ILayoutAlgorithm;
import rxlayout.LayoutManager;

using rxlayout.types.SizeBoxTools;
using rxlayout.types.LayoutOptions;


class Algorithm_Stacked_NoMargins_Test extends Test {

    function configuration( layout_size : LayoutDimension ) : LayoutInputConfiguration {
        return {
            options: {
                type: Stacked,
                align: Start,
                marginStart: 0.0,
                marginEnd: 0.0,
                gap: 0.0,
                base: 1.0,
            },
            layout_size: layout_size,
            packed_kids_length: 0.0,
            preferred_size_of_kids: [],
        }
    }

    function testCalculateAvailableLayoutSize() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : LayoutDimension;


        //
        // zero length
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r );


        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.marginStart = -2;
        config.options.marginEnd = -4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r );


        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.marginStart = 8;
        config.options.marginEnd = -4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r );


        //
        // non-zero length
        //

        config = configuration({ distance: 0.0, length: 16.0 });
        config.options.marginStart = 2;
        config.options.marginEnd = 4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 10.0 }, r );



        config = configuration({ distance: 0.0, length: 8.0 });
        config.options.marginStart = -2;
        config.options.marginEnd = -4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 8.0 }, r );


        config = configuration({ distance: 0.0, length: 8.0 });
        config.options.marginStart = 2;
        config.options.marginEnd = -4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 6.0 }, r );


        config = configuration({ distance: 0.0, length: 12.0 });
        config.options.marginStart = 8;
        config.options.marginEnd = 4;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r );


        config = configuration({ distance: 0.0, length: 12.0 });
        config.options.marginStart = 8;
        config.options.marginEnd = 6;
        r = algorithm.calculateAvailableLayoutSize( config.layout_size, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r );
    }
}


class Algorithm_Stacked_NoMargins_NoGap_Test extends Test {

    function configuration( layout_size : LayoutDimension ) : LayoutInputConfiguration {
        return {
            options: {
                type: Stacked,
                align: Start,
                marginStart: 0.0,
                marginEnd: 0.0,
                gap: 0.0,
                base: 1.0,
            },
            layout_size: layout_size,
            packed_kids_length: 0.0,
            preferred_size_of_kids: [],
        }
    }

    function testDefaults() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        config = configuration({ distance: 0.0, length: 0.0 });
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 0, r.length );
        Assert.same( [], r );
    }

    function testPx() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 128.0 });
        config.options.gap = 0;
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);
    }

    function testPercent() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 16.0 });
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 8.0 }, r[1]);
        Assert.same( { distance: 4.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 4.0, length: 8.0 }, r[3]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
    }

    function testBase() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 0.base(), length: 4.base() },
            { distance: 2.base(), length: 0.base() },
            { distance: 2.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 12.0 });
        config.options.base = 4;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 1.base(), length: 1.base() },
            { distance: 2.base(), length: 2.base() },
            { distance: 3.base(), length: 3.base() },
            { distance: 4.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        // trace( r );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 3.0, length: 3.0 }, r[1]);
        Assert.same( { distance: 6.0, length: 6.0 }, r[2]);
        Assert.same( { distance: 9.0, length: 9.0 }, r[3]);
        Assert.same( { distance: 12.0, length: 12.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 0.base(), length: 4.base() },
            { distance: 2.base(), length: 0.base() },
            { distance: 2.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
    }

    function testAuto() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 8.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 8.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);
    }

    function testUndefined() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);
    }

    function testMixed() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 11.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 3.0, length: 3.0 }, r[2]);
        Assert.same( { distance: 6.0, length: 6.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);
    }
}


class Algorithm_Stacked_NoMargins_WithGap_Test extends Test {

    function configuration( layout_size : LayoutDimension ) : LayoutInputConfiguration {
        return {
            options: {
                type: Stacked,
                align: Start,
                marginStart: 0.0,
                marginEnd: 0.0,
                gap: 4.0,
                base: 1.0,
            },
            layout_size: layout_size,
            packed_kids_length: 0.0,
            preferred_size_of_kids: [],
        }
    }

    function testDefaults() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        config = configuration({ distance: 0.0, length: 0.0 });
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 0, r.length );
        Assert.same( [], r );
    }

    function testPx() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.gap = 64;
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 128.0 });
        config.options.gap = 64;
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.gap = 64;
        config.preferred_size_of_kids = [
            { distance: 0.px(), length: 0.px() },
            { distance: 0.px(), length: 16.px() },
            { distance: 8.px(), length: 0.px() },
            { distance: 8.px(), length: 16.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, r[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, r[3]);
    }

    function testPercent() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.gap = 8;
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 16.0 });
        config.options.gap = 8;
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 8.0 }, r[1]);
        Assert.same( { distance: 4.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 4.0, length: 8.0 }, r[3]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.gap = 8;
        config.preferred_size_of_kids = [
            { distance: 0.percent(), length: 0.percent() },
            { distance: 0.percent(), length: 50.percent() },
            { distance: 25.percent(), length: 0.percent() },
            { distance: 25.percent(), length: 50.percent() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
    }

    function testBase() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.base = 4;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 0.base(), length: 4.base() },
            { distance: 2.base(), length: 0.base() },
            { distance: 2.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 11.0 });
        config.options.gap = 1;
        config.options.base = 4;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 1.base(), length: 1.base() },
            { distance: 2.base(), length: 2.base() },
            { distance: 3.base(), length: 3.base() },
            { distance: 4.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 2.75, length: 2.0 }, r[1]);
        Assert.same( { distance: 5.5, length: 5.0 }, r[2]);
        Assert.same( { distance: 8.25, length: 8.0 }, r[3]);
        Assert.same( { distance: 11.0, length: 11.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.base = 4;
        config.preferred_size_of_kids = [
            { distance: 0.base(), length: 0.base() },
            { distance: 0.base(), length: 4.base() },
            { distance: 2.base(), length: 0.base() },
            { distance: 2.base(), length: 4.base() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
    }

    function testAuto() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.options.gap = 2;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 4.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[4]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.options.gap = 2;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 2.px(), length: 4.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 4.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[4]);


        //
        // Parent size UNDEFINE
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.options.gap = 2;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.options.gap = 2;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 2.px(), length: 4.px() },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[4]);
    }

    function testUndefined() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 0.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 4, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);


        config = configuration( LayoutDimension.UNDEFINED );
        config.preferred_size_of_kids = [
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
            { distance: 0.px(), length: 4.px() },
            { distance: 2.px(), length: 0.px() },
            { distance: 2.px(), length: 4.px() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 2.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 2.0, length: 4.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);
    }

    function testMixed() {


        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var r : Array<LayoutDimension>;


        //
        // Parent size SET
        //

        config = configuration({ distance: 0.0, length: 0.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        config = configuration({ distance: 0.0, length: 24.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        // gap * n_kids > parent_length ==> kid_length = 0
        Assert.same( { distance: 3.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 6.0, length: 6.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        config = configuration({ distance: 0.0, length: 48.0 });
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 13.5 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 6.0, length: 2.5 }, r[2]);
        Assert.same( { distance: 12.0, length: 12.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);


        //
        // Parent size UNDEFINED
        //

        config = configuration( LayoutDimension.UNDEFINED );
        config.options.base = 8;
        config.preferred_size_of_kids = [
            { distance: SizeValue.AUTO, length: SizeValue.AUTO },
            { distance: 0.px(), length: 4.px() },
            { distance: 1.base(), length: 1.base() },
            { distance: 25.percent(), length: 25.percent() },
            { distance: SizeValue.UNDEFINED, length: SizeValue.UNDEFINED },
        ];
        r = algorithm.calculateKidsLayoutDimension( config );

        Assert.equals( 5, r.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r[0]);
        Assert.same( { distance: 0.0, length: 4.0 }, r[1]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[2]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[3]);
        Assert.same( { distance: 0.0, length: 0.0 }, r[4]);
    }
}
