// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package algorithm;

import utest.Assert;
import utest.Test;

import rxlayout.algorithm.LayoutDimensionSize;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


class LayoutDimensionSizeTest extends Test {

    function testDefaults() {

        var lds : LayoutDimensionSize;

        Assert.isTrue( LayoutDimensionSize.ZERO != LayoutDimensionSize.ZERO );
        Assert.isTrue( LayoutDimensionSize.UNDEFINED != LayoutDimensionSize.UNDEFINED );

        lds = LayoutDimensionSize.ZERO;
        Assert.same( 0.px(), lds.distance );
        Assert.same( 0.px(), lds.length );

        lds = LayoutDimensionSize.UNDEFINED;
        Assert.isTrue( lds.distance.isUndefined() );
        Assert.isTrue( lds.distance.isUndefined() );
    }
}
