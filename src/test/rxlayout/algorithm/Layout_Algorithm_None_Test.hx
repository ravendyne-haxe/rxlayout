// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package algorithm;

import utest.Assert;
import utest.Test;

import rxlayout.types.Constants;
import rxlayout.algorithm.LayoutDimension;
import rxlayout.algorithm.ILayoutAlgorithm;
import rxlayout.LayoutManager;

using rxlayout.types.SizeBoxTools;
using rxlayout.types.LayoutOptions;


class Layout_Algorithm_None_Test extends Test {

    function configuration( align : AxisAlignment, layout_size : LayoutDimension ) : LayoutInputConfiguration {
        return {
            options: {
                type: None,
                align: align,
                marginStart: 0.0,
                marginEnd: 0.0,
                gap: 0.0,
                base: 1.0,
            },
            layout_size: layout_size,
            packed_kids_length: 0.0,
            preferred_size_of_kids: [],
        }
    }

    function testNoKids() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var kids_boxes : Array<LayoutDimension>;
        var r : LayoutOutputConfiguration;


        kids_boxes = [];


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.equals( 0, r.kids_dimensions.length );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 8.0;
        config.options.gap = 0;
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.equals( 0, r.kids_dimensions.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 4;
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.equals( 0, r.kids_dimensions.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 8.0;
        config.options.gap = 4;
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.equals( 0, r.kids_dimensions.length );
        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
    }

    function testWithKidsStart() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var kids_boxes : Array<LayoutDimension>;
        var r : LayoutOutputConfiguration;
        var k : Array<LayoutDimension>;


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 4;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: 0.0, length: 24.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: Const.UNDEFINED_VALUE, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: Const.UNDEFINED_VALUE, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( Start, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 12;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);
    }

    function testWithKidsCenter() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var kids_boxes : Array<LayoutDimension>;
        var r : LayoutOutputConfiguration;
        var k : Array<LayoutDimension>;


        config = configuration( Center, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 4;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: 0.0, length: 24.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: Const.UNDEFINED_VALUE, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: Const.UNDEFINED_VALUE, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( Center, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 12;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);
    }

    function testWithKidsEnd() {

        final algorithm = LayoutManager.getLayoutAlgorithm();
        var config : LayoutInputConfiguration;
        var kids_boxes : Array<LayoutDimension>;
        var r : LayoutOutputConfiguration;
        var k : Array<LayoutDimension>;


        config = configuration( End, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 4;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: 0.0, length: 24.0 } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: Const.UNDEFINED_VALUE, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 0.0;
        config.options.marginEnd = 0.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: Const.UNDEFINED_VALUE, length: 24.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 0.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 0.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 8.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 8.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: 0.0, length: 0.0 } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 0.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 0;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);


        config = configuration( End, { distance: 0.0, length: Const.UNDEFINED_VALUE } );
        config.options.marginStart = 4.0;
        config.options.marginEnd = 2.0;
        config.options.gap = 12;
        kids_boxes = [
            { distance: 0.0, length: 0.0 },
            { distance: 0.0, length: 16.0 },
            { distance: 8.0, length: 0.0 },
            { distance: 8.0, length: 16.0 },
        ];
        r = algorithm.calculateLayout( config.layout_size, kids_boxes, config.options );

        Assert.same( { distance: 0.0, length: 30.0 }, r.layout_size );
        k = r.kids_dimensions;
        Assert.equals( 4, k.length );
        Assert.same( { distance: 4.0, length: 0.0 }, k[0]);
        Assert.same( { distance: 4.0, length: 16.0 }, k[1]);
        Assert.same( { distance: 12.0, length: 0.0 }, k[2]);
        Assert.same( { distance: 12.0, length: 16.0 }, k[3]);
    }
}
