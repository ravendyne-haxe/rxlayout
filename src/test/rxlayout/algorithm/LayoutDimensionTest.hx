// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package algorithm;

import utest.Assert;
import utest.Test;

import rxlayout.types.SizeBox;
import rxlayout.algorithm.LayoutDimension;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


class LayoutDimensionTest extends Test {

    function testDefaults() {

        var ld : LayoutDimension;

        Assert.isTrue( LayoutDimension.ZERO != LayoutDimension.ZERO );
        Assert.isTrue( LayoutDimension.UNDEFINED != LayoutDimension.UNDEFINED );

        Assert.same( { distance: 0.0, length: 0.0 }, LayoutDimension.ZERO );

        ld = LayoutDimension.UNDEFINED;
        Assert.isFalse( ld.distance.isNumber() );
        Assert.isFalse( ld.length.isNumber() );
    }
}
