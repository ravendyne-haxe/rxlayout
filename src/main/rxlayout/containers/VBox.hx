// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.containers;

import rxlayout.types.IViewElement;

class VBox extends HVBox {
    public function new( gap : Float = 0.0, ?viewElement : IViewElement ) {
        super( gap, viewElement );

        layoutParameters.direction = Column;
    }
}
