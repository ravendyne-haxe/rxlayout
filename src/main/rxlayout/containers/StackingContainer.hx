// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.containers;

import rxlayout.types.IViewElement;
import rxlayout.base.LayoutContainer;

class StackingContainer extends LayoutContainer {
    public function new( ?viewElement : IViewElement ) {
        super( viewElement );

        layoutParameters.direction = Row;
        layoutParameters.mainAxis.type = Aligned;
        layoutParameters.mainAxis.align = Start;
        layoutParameters.crossAxis.type = Aligned;
        layoutParameters.crossAxis.align = Start;
        margins = 0.0;
    }
}
