// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.containers;

import rxlayout.types.IViewElement;
import rxlayout.base.LayoutContainer;

class HVBox extends LayoutContainer {
    public function new( gap : Float = 0.0, ?viewElement : IViewElement ) {
        super( viewElement );

        layoutParameters.mainAxis.type = Stacked;
        layoutParameters.mainAxis.align = Start;
        layoutParameters.mainAxis.gap = gap;
        layoutParameters.crossAxis.type = Aligned;
        layoutParameters.crossAxis.align = Start;
        layoutParameters.crossAxis.gap = 0.0;
        margins = 0.0;
    }
}
