// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.containers;

import rxlayout.types.IViewElement;
import rxlayout.base.LayoutContainer;

class Container extends LayoutContainer {
    public function new( ?viewElement : IViewElement ) {
        super( viewElement );

        layoutParameters.mainAxis.type = None;
        layoutParameters.crossAxis.type = None;
        margins = 0.0;
    }
}
