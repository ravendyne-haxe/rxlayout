// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.elements;

import rxlayout.types.LayoutBox;
import rxlayout.types.IViewElement;

class DummyViewElement implements IViewElement {
    public function new() {}
    public function setBox( box : LayoutBox ) : Void {}
    public function getBox() : LayoutBox { return LayoutBox.ZERO; }
}
