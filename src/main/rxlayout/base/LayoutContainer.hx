// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.base;

import haxe.Exception;

import rxlayout.algorithm.LayoutDimensionSize;
import rxlayout.algorithm.LayoutDimension;
import rxlayout.types.IViewElement;
import rxlayout.base.LayoutContainerBase;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.SizeBox;
import rxlayout.types.LayoutBox;
import rxlayout.types.LayoutOptions;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;

typedef LayoutParameters = {
    var direction : LayoutDirection;
    var mainAxis : LayoutOptions;
    var crossAxis : LayoutOptions;
}

typedef CalculatedLayout = {
    layout_size: LayoutBox,
    kids_boxes: Array<LayoutBox>,
}

/**
    This is the base class for implementing specialized layout containers.

    Extending classes need to at minimum override `applyLayout()` method which
    should contain implementation of the classe's layout algorithm.

    The class constructor accepts optional `IViewElement` that is meant to somehow
    represent this layout container in the rendered UI. Layout containers don't
    have to have renderable representation in UI, but if they do (i.e. UI panels)
    the UI element can be assigned via the constructor parameter and its size/position
    will be driven by `ViewElemen.setBox()` method.
**/
class LayoutContainer extends LayoutContainerWithParamters {

    public function new( ?viewElement : IViewElement ) {
        super();

        if( viewElement != null ) _viewElement = viewElement;
    }

    public function addChildren( children : Array<ILayoutContainer> ) : Void {
        for( c in children )  {
            _children.push( c );
            c.parent = this;
        }
        updatePackedSize();
    }

    public function removeChildren( children : Array<ILayoutContainer> ) : Void {
        for( c in children ) {
            _children.remove( c );
            c.parent = this;
        }
        updatePackedSize();
    }

    public function updatePackedSize() {
        // - this is size for kids for when we have no advised size (our layout_box)
        // - kids with %, base, ... size types will have size 0
        // - only uses px values, if set
        // - this **must** be a valid number


        // - if a dimension of my assigned_box is in px, use that
        //      ( because `UNDEFINED.assignBoxFromSizeBox()` returns PX if assigned_box dimension
        //          is PX, and UNDEFINED otherwise )
        var layout_box = LayoutBox.UNDEFINED.assignBoxFromSizeBox( _assigned_box );

        // - ask kids their packed_size
        // var preferred_size_of_kids = getPreferredKidsSizes( layout_box );
        var preferred_size_of_kids = [ for( child in _children ) child.getPreferredBox() ];

        // - calc my packed_size based on kids and my layout algo
        var calculated_layout = applyLayout( layout_box, LayoutBox.ZERO, preferred_size_of_kids );

        packed_size = LayoutBox.ZERO.assignSizeIfRightIsSet( calculated_layout.layout_size );

        if( _parent != null )
            _parent.updatePackedSize();
    }

    function getPreferredKidsSizes( layout_box : LayoutBox ) : Array<SizeBox> {
        var preferred_size_of_kids : Array<SizeBox> = [];
        for( child in _children ) {
            var kid_pref_box = child.getPreferredBox();
            // TODO use `child.packed_size` as min_size for kid,
            //      if layout_box.x/y/width/height is UNDEFINED
            //          use child.packed_size.x/y/width/height
            //      otherwise
            //          use kid_pref_box.x/y/width/height
            // preferred_size_of_kids.push( child.packed_size );
            preferred_size_of_kids.push( kid_pref_box );
        }
        return preferred_size_of_kids;
    }

    public function doLayout( your_advised_layout_box : LayoutBox ) : LayoutBox {

        // a current_layout_box size dimension (width, height) will get value
        // from _assigned_box ONLY if the value type is PX, otherwise it will be UNDEFINED
        var current_layout_box = LayoutBox.UNDEFINED.assignSizeFromSizeBox( _assigned_box );
        // a current_layout_box dimension (x, y, width, height) will get value
        // from your_advised_layout_box ONLY if it is UNDEFINED
        current_layout_box.assignBoxIfLeftNotSet( your_advised_layout_box );

        // at this point, if current_layout_box.width/height dimension is:
        //  - UNDEFINED: that means kids are driving our final layout size
        //      - also, kids having a size of type PERCENT, AUTO, BASE or UNDEFINED will end up
        //          with the size value of 0
        //  - set to a value: that means we have our final layout size set and we are driving kids
        //      - also, kids size values of type PERCENT, AUTO and BASE have meaning because we have a valid numeric
        //          dimension size ourselves (width and/or height) and handling of kid size value of
        //          UNDEFINED depends of specific layout algorithm, and SHOULD be set to 0 in general case.


        // - collect kids sizes
        //     - ask kid its pref_size
        // var preferred_size_of_kids = getPreferredKidsSizes( current_layout_box );
        var preferred_size_of_kids = [ for( child in _children ) child.getPreferredBox() ];

        // - calc x,y,h,w for kids
        //     - using layout algo
        //     - taking into account px, %, BASE, ...
        var calculated_layout = applyLayout( current_layout_box, packed_size, preferred_size_of_kids );
        // - advise kids on their calc'd size
        //     - kid.do_layout( your_advised_size )
        for( idx in 0..._children.length ) {
            var child = _children[idx];
            child.doLayout( calculated_layout.kids_boxes[idx] );
        }

        // unless we already have a dimension set on our current layou size,
        // set it from the value calculated by the layout algorithm.
        current_layout_box.assignBoxIfLeftNotSet( calculated_layout.layout_size );


        // at this point current_layout_box **must** have valid numerical values for x,y, width AND height
        // if it hasn't already, we will set it now to 0
        // first, do we have our own values?
        current_layout_box.assignBoxIfLeftNotSet( _assigned_box );
        // if not, set to zero whatever is UNDEFINED
        current_layout_box.assignBoxIfLeftNotSet( LayoutBox.ZERO );

        // - set my layout_box
        layout_box = current_layout_box;
        // - update my view_element
        _viewElement.setBox( layout_box );

        // - return my new layout position & size
        return layout_box;
    }

    /**
        Contains implementation of this container's layout algorithm. Should be overriden in
        extending classes.

        If `layout_size` parameter contains valid values for `width` or `height` properties,
        those represent amount of space available to this container instance and
        should be used as a constraint while laying out child containers.

        This mode (when a layout dimension has valid value) is also called "_parent driven layout_".

        If `layout_size` parameter contains undefined values for `width` or `height` properties,
        this container needs to use its children's, if any, valid size values and then
        to position/size the children according to its layout algorithm.
        The size that children containers take up after being laid out in this way should be
        returned as a layout box after layout algorithm has been applied, and essentially represents
        what is contained in the `packed_size` container property.

        This mode (when a layout dimension has undefined value) is also called "_children driven layout_".
    **/
    function applyLayout( layout_size : LayoutBox, packed_kids_size : LayoutBox, preferred_size_of_kids : Array<SizeBox> ) : CalculatedLayout {
        // layout_size width/height is either UNDEFINED or has set value
        // - if it is UNDEFINED, we get to set it from kids sizes and our layout algorithm
        // - if it HAS VALUE, we need to fit kids into that space usign our layout algorithm

        //
        // SPLIT all dimensions by main and cross axis
        //
        var main_layout_size : LayoutDimension;
        var cross_layout_size : LayoutDimension;
        var main_packed_kids_size : LayoutDimension;
        var cross_packed_kids_size : LayoutDimension;
        var main_preferred_size_of_kids : Array<LayoutDimensionSize> = [];
        var cross_preferred_size_of_kids : Array<LayoutDimensionSize> = [];

        switch( layoutParameters.direction ) {
            case Row:
                main_layout_size = LayoutDimension.fromLayoutBox( layout_size, Row );
                cross_layout_size = LayoutDimension.fromLayoutBox( layout_size, Column );
                main_packed_kids_size = LayoutDimension.fromLayoutBox( packed_kids_size, Row );
                cross_packed_kids_size = LayoutDimension.fromLayoutBox( packed_kids_size, Column );
                main_preferred_size_of_kids = [ for( ks in preferred_size_of_kids ) LayoutDimensionSize.fromSizeBox( ks, Row ) ];
                cross_preferred_size_of_kids = [ for( ks in preferred_size_of_kids ) LayoutDimensionSize.fromSizeBox( ks, Column ) ];

            case Column:
                main_layout_size = LayoutDimension.fromLayoutBox( layout_size, Column );
                cross_layout_size = LayoutDimension.fromLayoutBox( layout_size, Row );
                main_packed_kids_size = LayoutDimension.fromLayoutBox( packed_kids_size, Column );
                cross_packed_kids_size = LayoutDimension.fromLayoutBox( packed_kids_size, Row );
                main_preferred_size_of_kids = [ for( ks in preferred_size_of_kids ) LayoutDimensionSize.fromSizeBox( ks, Column ) ];
                cross_preferred_size_of_kids = [ for( ks in preferred_size_of_kids ) LayoutDimensionSize.fromSizeBox( ks, Row ) ];
        }

        //
        // CALCULATE LAYOUT
        //

        var r = LayoutManager.getLayoutAlgorithm().calculate({
            options: layoutParameters.mainAxis,
            layout_size: main_layout_size,
            packed_kids_length: main_packed_kids_size.length,
            preferred_size_of_kids: main_preferred_size_of_kids,
        },{
            options: layoutParameters.crossAxis,
            layout_size: cross_layout_size,
            packed_kids_length: cross_packed_kids_size.length,
            preferred_size_of_kids: cross_preferred_size_of_kids,
        });

        //
        // COMBINE all dimensions from main and cross axis
        //
        var calculated_layout_size: LayoutBox;
        var horizontal_kids_dimensions: Array<LayoutDimension>;
        var vertical_kids_dimensions: Array<LayoutDimension>;
        var main_axis_layout = r.mainAxisResult;
        var cross_axis_layout = r.crossAxisResult;

        switch( layoutParameters.direction ) {
            case Row:
                calculated_layout_size = {
                    x: layout_size.x,
                    y: layout_size.y,
                    width: main_axis_layout.layout_size.length,
                    height: cross_axis_layout.layout_size.length,
                }
                horizontal_kids_dimensions = main_axis_layout.kids_dimensions;
                vertical_kids_dimensions = cross_axis_layout.kids_dimensions;

            case Column:
                calculated_layout_size = {
                    x: layout_size.x,
                    y: layout_size.y,
                    width: cross_axis_layout.layout_size.length,
                    height: main_axis_layout.layout_size.length,
                }
                horizontal_kids_dimensions = cross_axis_layout.kids_dimensions;
                vertical_kids_dimensions = main_axis_layout.kids_dimensions;

        }

        var calculated_kids_boxes: Array<LayoutBox> = [];
        // both horizontal_kids_dimensions & vertical_kids_dimensions MUST have same length
        for( idx in 0...horizontal_kids_dimensions.length ) {
            var h_d = horizontal_kids_dimensions[ idx ];
            var v_d = vertical_kids_dimensions[ idx ];
            calculated_kids_boxes.push({
                x: h_d.distance,
                y: v_d.distance,
                width: h_d.length,
                height: v_d.length,
            });
        }

        //
        // FINAL RESULT
        //
        return {
            layout_size: calculated_layout_size,
            kids_boxes: calculated_kids_boxes,
        }
    }
}
