// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.base;

import rxlayout.types.ILayoutContainer;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutContainerBase;
import rxlayout.types.IViewElement;

/**
    This is the base class for implementing layout container elements.

    A layout container element is meant to represent visible, drawable UI elements
    (buttons, inputs, panels, etc.) which are being positioned and resized (a.k.a. laid out)
    by the UI layout tree.

    UI layout tree always has one root layout container which then contains a number of
    children which may contain more children, etc. Leafs of this tree are usually UI elements
    that are meant to be drawn and presented to the user.

    The `LayoutElement` class represents that leaf node.

    Since every node of a layout tree must implement `ILayoutContainer` interface, so must
    also every leaf in the tree that simply represents an UI element. However, these leaf nodes,
    by definition of a leaf node, can't have children and, also, are not implementing any
    layout algorithm. `LayoutElement` class implements all of those three requirements:
    it's a `ILayoutContainer`, you can't add other layout containers as children to it, and
    it doesn't implement any layout algorithm.

    It basically provides a way to associate an actual UI element with it via its constructor
    parameter.

    Whenever `LayoutElement`'s `assigned_box` is set, it will update its view element's box.
    The other way around does *not* work: setting view elements position/size will *not*
    update `LayoutElement`'s `assigned_box`.
**/
class LayoutElement extends LayoutContainerBase {

    public function new( viewElement : IViewElement ) {
        super();

        _viewElement = viewElement;
    }

    public function updatePackedSize() {
        // Generic packed size calculation that supports layout element spec:
        //  - use assigned_box if its type is PX, or
        //  - use ZERO, (because ZERO.assignBoxFromSizeBox() will return 0 for `%`, `BASE` and `AUTO` dimension types)
        packed_size = LayoutBox.ZERO.assignSizeFromSizeBox( _assigned_box );

        if( _parent != null )
            _parent.updatePackedSize();
    }


    public function doLayout( your_advised_layout_box : LayoutBox ) : LayoutBox {

        // layout container elements don't do any layout, because they are leaf
        // nodes in a layout container hierarchy. so implementation here simply adjusts
        // element size based on advised box sent from parent.

        // we only change assigned_box if advised box contains valid values

        // _assigned_box.assignBoxIfRightIsSet( your_advised_layout_box );
        // updatePackedSize();
        // _viewElement.setBox( _assigned_box );
        _viewElement.setBox( your_advised_layout_box );
        // layout_box.assignBox( your_advised_layout_box ).assignSizeIfRightIsSet( packed_size );
        layout_box.assignBox( your_advised_layout_box );

        return layout_box;
    }

    public function addChildren( children : Array<ILayoutContainer> ) : Void {}

    public function removeChildren( children : Array<ILayoutContainer> ) : Void {}
}
