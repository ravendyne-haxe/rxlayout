// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.base;

import rxlayout.base.LayoutContainer.LayoutParameters;
import rxlayout.types.LayoutOptions;

using rxlayout.RxLayoutUtil;

/**
 * This class is just so we can organize source code a bit better.
 *
 * It contains the `layoutParameters` property and all the various
 * getters/setters for different `LayoutParameters` properties.
 */
abstract class LayoutContainerWithParamters extends LayoutContainerBase {

    private var layoutParameters : LayoutParameters;

    //
    // MARGINS
    //
    public var margins(null,set) : Float;
    function set_margins( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        layoutParameters.mainAxis.marginStart = v;
        layoutParameters.mainAxis.marginEnd = v;
        layoutParameters.crossAxis.marginStart = v;
        layoutParameters.crossAxis.marginEnd = v;
        return v;
    }

    public var marginTop(null,set) : Float;
    function set_marginTop( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.marginStart = v;
            case Column: layoutParameters.mainAxis.marginStart = v;
        }
        return v;
    }

    public var marginBottom(null,set) : Float;
    function set_marginBottom( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.marginEnd = v;
            case Column: layoutParameters.mainAxis.marginEnd = v;
        }
        return v;
    }

    public var marginLeft(null,set) : Float;
    function set_marginLeft( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.marginStart = v;
            case Column: layoutParameters.crossAxis.marginStart = v;
        }
        return v;
    }

    public var marginRight(null,set) : Float;
    function set_marginRight( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.marginEnd = v;
            case Column: layoutParameters.crossAxis.marginEnd = v;
        }
        return v;
    }

    //
    // GAPS
    //
    public var gaps(null,set) : Float;
    function set_gaps( v : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        layoutParameters.mainAxis.gap = v;
        layoutParameters.crossAxis.gap = v;
        return v;
    }

    public var horizontalGap(get,set) : Float;
    function get_horizontalGap() : Float {
        return switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.gap;
            case Column: layoutParameters.crossAxis.gap;
        }
    };
    function set_horizontalGap( v  : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.gap = v;
            case Column: layoutParameters.crossAxis.gap = v;
        }
        return v;
    };

    public var verticalGap(get,set) : Float;
    function get_verticalGap() : Float {
        return switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.gap;
            case Column: layoutParameters.mainAxis.gap;
        }
    };
    function set_verticalGap( v  : Float ) : Float {
        if( ! v.isNumber() ) v = 0.0;
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.gap = v;
            case Column: layoutParameters.mainAxis.gap = v;
        }
        return v;
    };

    //
    // ALIGNMENT
    //
    public var horizontalAlignment(get,set) : AxisAlignment;
    function get_horizontalAlignment() : AxisAlignment {
        return switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.align;
            case Column: layoutParameters.crossAxis.align;
        }
    };
    function set_horizontalAlignment( v  : AxisAlignment ) : AxisAlignment {
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.mainAxis.align = v;
            case Column: layoutParameters.crossAxis.align = v;
        }
        return v;
    };

    public var verticalAlignment(get,set) : AxisAlignment;
    function get_verticalAlignment() : AxisAlignment {
        return switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.align;
            case Column: layoutParameters.mainAxis.align;
        }
    };
    function set_verticalAlignment( v  : AxisAlignment ) : AxisAlignment {
        switch( layoutParameters.direction ) {
            case Row: layoutParameters.crossAxis.align = v;
            case Column: layoutParameters.mainAxis.align = v;
        }
        return v;
    };

    //
    // CONSTRUCTOR
    //
    // sets defaults for all layout parameters
    public function new() {
        super();

        layoutParameters = {
            direction: Row,
            mainAxis: {
                type: None,
                align: Start,
                marginStart: 0,
                marginEnd: 0,
                gap: 0,
                base: 1,
            },
            crossAxis: {
                type: None,
                align: Start,
                marginStart: 0,
                marginEnd: 0,
                gap: 0,
                base: 1,
            },
        }
    }

}
