// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.base;

import rxlayout.types.SizeValue;
import rxlayout.elements.DummyViewElement;
import rxlayout.types.ILayoutContainer;
import rxlayout.types.IViewElement;
import rxlayout.types.SizeBox;
import rxlayout.types.LayoutBox;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


abstract class LayoutContainerBase implements ILayoutContainer {

    public var view_element(get,set) : IViewElement;
    private function get_view_element() : IViewElement {
        return _viewElement;
    }
    private function set_view_element( v : IViewElement ) : IViewElement {
        _viewElement = v;
        return _viewElement;
    }
    var _viewElement : IViewElement;

    // assigned_box
    //  - x,y,w,h
    //  - value: Float
    //  - type: px, %, base, ...
    // px -> this is my size, no matter what
    // % -> this is how much I want to take of the space my parent gives me
    // base -> used by parent in flex layout, otherwise same as TUNDEFINED
    // undefined -> I have no desired size set
    public var assigned_box(get,null) : SizeBox;
    private function get_assigned_box() : SizeBox {
        return _assigned_box.clone();
    }
    var _assigned_box : SizeBox;
    public function setPositionAndSizeBox( box : SizeBox ) : Void {
        _assigned_box = box.clone();
        updatePackedSize();
    }
    public function setPositionAndSize( x : SizeValue, y : SizeValue, width : SizeValue, height : SizeValue ) {
        _assigned_box.x = x;
        _assigned_box.y = y;
        _assigned_box.width = width;
        _assigned_box.height = height;
        updatePackedSize();
    }
    public function setPosition( x : SizeValue, y : SizeValue ) {
        _assigned_box.x = x;
        _assigned_box.y = y;
        updatePackedSize();
    }
    public function setSize( width : SizeValue, height : SizeValue ) {
        _assigned_box.width = width;
        _assigned_box.height = height;
        updatePackedSize();
    }

    public function getPreferredBox() : SizeBox {
        // - if my assigned_box is set (px, %, BASE, ...)
        //     return that
        // var preferred_size = SizeBox.UNDEFINED.assignSizeIfRightIsSet( _assigned_box );
        var preferred_size = SizeBox.UNDEFINED.assignBoxIfRightIsSet( _assigned_box );
        // otherwise:
        // - return packed_size
        preferred_size.assignBoxIfLeftIsUndefined( packed_size );

        return preferred_size;
    }

    // packed_size : LayoutBox
    //  - w,h
    // this is minimum size required to contain all the children
    public var packed_size(default,null) : LayoutBox;

    // @implement public function updatePackedSize();


    public function new() {

        _assigned_box = SizeBox.UNDEFINED;
        packed_size = LayoutBox.ZERO;
        layout_box = LayoutBox.UNDEFINED;
        _children = [];

        _viewElement = new DummyViewElement();

        _parent = null;
    }



    // layout_box : LayoutBox
    //  - x,y,w,h
    // container size calculated by the most recent call to doLayout()
    public var layout_box(default,null) : LayoutBox;

    // @implement public function doLayout( your_advised_layout_box : LayoutBox ) : LayoutBox;

    public var parent(get,set) : ILayoutContainer;
    var _parent : ILayoutContainer;
    private function get_parent() : ILayoutContainer { return _parent; }
    private function set_parent( p : ILayoutContainer ) : ILayoutContainer { _parent = p; return _parent; }

    public var children(get,never) : Iterator<ILayoutContainer>;
    var _children : Array<ILayoutContainer>;
    function get_children() {
        return _children.iterator();
    }

    // @implement function addChildren( children : Array<ILayoutContainer> ) : Void;
    // @implement function removeChildren( children : Array<ILayoutContainer> ) : Void;
}
