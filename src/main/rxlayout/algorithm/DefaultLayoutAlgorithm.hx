// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.algorithm;

import haxe.Exception;
import rxlayout.types.LayoutOptions;
import rxlayout.algorithm.ILayoutAlgorithm;
import rxlayout.algorithm.LayoutDimensionSize;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;

using rxlayout.algorithm.DimensionTools;


class DefaultLayoutAlgorithm implements ILayoutAlgorithm {
    public function new() {}

    public function calculate( mainAxisConfiguration: LayoutInputConfiguration, crossAxisConfiguration: LayoutInputConfiguration ) : CalculatedLayoutResult {

        //
        // CALCULATE LAYOUT
        //

        // convert every kid's SizeBox into concrete value of LayoutBox type,
        var main_kids_boxes = calculateKidsLayoutDimension( mainAxisConfiguration );
        var cross_kids_boxes = calculateKidsLayoutDimension( crossAxisConfiguration );

        var main_axis_layout = calculateLayout( mainAxisConfiguration.layout_size, main_kids_boxes, mainAxisConfiguration.options );
        var cross_axis_layout = calculateLayout( crossAxisConfiguration.layout_size, cross_kids_boxes, crossAxisConfiguration.options );

        if( main_axis_layout.kids_dimensions.length != cross_axis_layout.kids_dimensions.length ) throw new Exception("'calculateLayout()' BUG!");

        return {
            mainAxisResult: main_axis_layout,
            crossAxisResult: cross_axis_layout,
        }
    }

    /**
     * In order to convert every kid's `LayoutDimensionSize` into concrete value of `LayoutDimension` type,
     * we need to determine space that will be available to kids once margins are
     * subtracted from the avaliable layout space.
     *
     * If a kid has a dimension of `PERCENT` or `BASE` type, that dimension will be converted
     * to `PX` type based on the total available space returned by this function.
     *
     * @param concrete_layout_size `length` property **must** be valid number
     * @return `LayoutDimension` where `distance` = `0.0`, and `length` contains available space
     */
    public function calculateAvailableLayoutSize( concrete_layout_size : LayoutDimension, options : LayoutOptions ) : LayoutDimension {

        var marginTotal = Math.max( 0.0, options.marginStart ) + Math.max( 0.0, options.marginEnd );

        var available_size = concrete_layout_size.length - marginTotal;

        if( available_size < 0 ) available_size = 0.0;

        return {
            distance: 0.0,
            length: available_size
        };
    }

    /**
     * Converts kid's `LayoutDimensionSize` properties into concrete numbers using corresponding `concrete_layout_size`
     * parameter `length` property for `%` and `BASE` types.
     *
     * `concrete_layout_size` is calculated from `inputConfiguration.layout_size` based on `inputConfiguration.options.type` and
     * `inputConfiguration.packed_kids_length`.
     *  **Every** kid in `preferred_size_of_kids` is expected to have `length` property of either `PX`, `%`, `AUTO` or `BASE` type.
     *
     * @param inputConfiguration
     * @return Array<LayoutDimension>
     */
    public function calculateKidsLayoutDimension( inputConfiguration: LayoutInputConfiguration ) : Array<LayoutDimension> {

        var layout_size : LayoutDimension = inputConfiguration.layout_size;
        var packed_kids_length : Float = inputConfiguration.packed_kids_length;
        var preferred_size_of_kids : Array<LayoutDimensionSize> = inputConfiguration.preferred_size_of_kids;
        var options : LayoutOptions = inputConfiguration.options;

        // `ZERO.assignIfRightIsSet()` sets a dimension to either layout_size value,
        // or to `0` if layout_size value is UNDEFINED
        var concrete_layout_size = LayoutDimension.ZERO.assignIfRightIsSet( layout_size );
        if( ! layout_size.length.isNumber() ) {
            var packed_kids_length = packed_kids_length.isNumber() ? packed_kids_length : 0.0;
            concrete_layout_size.length = switch( inputConfiguration.options.type ) {
                // case None: concrete_layout_size.length;
                case None: packed_kids_length;
                case Aligned: packed_kids_length;
                // case Stacked: packed_kids_length;
                case Stacked: concrete_layout_size.length;
            }
        }

        // first, determine space available to divide amongst kids
        // by subtracting margins...
        var available_layout_size = calculateAvailableLayoutSize( concrete_layout_size, options );

        //
        // ...and then convert all `%`, `BASE` and `AUTO` type dimensions to `PX`, based
        // on that available space
        //

        // kid's 'distance' need to be calculated as `%` or `BASE` of layout's 'length'
        // so we create a special LayoutDimension in order to make those calculations.
        // in case available_layout_size.length is UNDEFINED, well set it to 0.
        var layout_values_for_kids = LayoutDimension.ZERO.assignIfRightIsSet({
            distance: available_layout_size.length,
            length: available_layout_size.length,
        });

        var unitBasePx = calculateBaseUnit( options.base, layout_values_for_kids.distance, 0.0 );
        var unitBasePlusGapPx = calculateBaseUnit( options.base, layout_values_for_kids.length, options.gap );

        var kids_boxes : Array<LayoutDimension> = [];
        var kids_to_update : Array<LayoutDimension> = [];
        var used_layout_length = 0.0;
        var last_auto_kid_index = -1;
        for( ks in preferred_size_of_kids ) {
            // `preferred_size_of_kids` should NEVER contain a kid that has ANY
            // of its dimensions set to `UNDEFINED` type.

            // `assignFromLayoutDimensionSize()` will either keep `PX` value type from a `ks` dimension, or
            // convert it to `PX` value as `%` of the corresponding `layout_values_for_kids`
            // dimension. We handle `BASE`, `UNDEFINED` and `AUTO` types below.
            var kid_box = layout_values_for_kids.clone().assignFromLayoutDimensionSize( ks );

            // handle `UNDEFINED` type values
            if( ks.distance.type == TUNDEFINED ) kid_box.distance = 0.0;
            if( ks.length.type == TUNDEFINED ) kid_box.length = 0.0;

            // handle `BASE` type values
            if( ks.distance.type == BASE ) kid_box.distance = Math.max( 0.0, ks.distance.getPxValueOf( unitBasePx ) );
            if( ks.length.type == BASE ) kid_box.length = Math.max( 0.0, ks.length.getPxValueOf( unitBasePlusGapPx ) - options.gap );

            // handle `AUTO` type values
            if( ks.distance.type == TAUTO ) kid_box.distance = 0.0;
            if( ks.length.type == TAUTO ) {
                kid_box.length = 0.0;
                kids_to_update.push( kid_box );
                last_auto_kid_index = kids_boxes.length;
            }
            // if( ks.length.type == TAUTO ) kid_box.length = auto_layout_length;
            // if( ks.length.type == TAUTO ) kid_box.length = ks.distance.getPxValueOf( auto_layout_length );

            // TODO adding margins to distance is accounted for in calculateLayout()
            // kid_box.distance += options.marginStart;

            if( ks.length.type != TAUTO ) used_layout_length += kid_box.length;

            kids_boxes.push( kid_box );
        }

        if( kids_to_update.length > 0 ) {
            var auto_layout_kid_count = kids_to_update.length;

            switch( options.type ) {
                case None:
                    // For None layout type we set length for AUTO kids so that
                    // they fill all space from their distance to the amount of available space
                    for( kb in kids_to_update ) {
                        var auto_layout_length = available_layout_size.length - kb.distance;
                        if( auto_layout_length > 0 ) {
                            kb.length = auto_layout_length;
                        }
                    }


                case Aligned:
                    // For Aligned layout type we set length to max available for AUTO kids

                    var auto_layout_length = available_layout_size.length;

                    if( auto_layout_length > 0 ) {
                        for( kb in kids_to_update ) {
                            kb.length = auto_layout_length;
                        }
                    }

                case Stacked:
                    // For Stacked layout type we divide remaining space equally among AUTO kids

                    // space available for AUTO kids is total layout space, minus whatever is already used,
                    // minus whatever is used by gaps. "preferred_size_of_kids.length" is number of kids, each
                    // gets one gap except for the last one, thus "preferred_size_of_kids.length - 1"
                    var auto_layout_length = available_layout_size.length - used_layout_length - ( preferred_size_of_kids.length - 1 ) * options.gap;
                    var auto_kid_length = Math.max( 0.0, auto_layout_length ) / auto_layout_kid_count;

                    if( auto_kid_length > 0 ) {
                        for( kb in kids_to_update ) {
                            kb.length = auto_kid_length;
                        }
                    }

            }

        }

        return kids_boxes;
    }

    function calculateBaseUnit( content_base : Float, content_space : Float, gap : Float ) : Float {
        /*
            content_space = space - ( start_margin + end_margin )

            content_base = integer number of "columns" the conten_space is divided into, >= 1.0
            gap_space = gap * ( content_base - 1 )

            base_unit = ( content_space - gap_space ) / content_base
            base_unit_plus_gap = base_unit + gap

            kid_length = kid_base * base_unit + ( kid_base - 1 ) * gap
                    = kid_base * ( base_unit + gap ) - gap
                    = kid_base * base_unit_plus_gap - gap

            kid_length = kid.distance_or_length.getPxValueOf( base_unit_plus_gap ) - gap
        */
        // var content_base = options.base.getValidBaseValue();
        content_base = Math.max( 1.0, content_base.getValidBaseValue() );
        var gap_space = gap * ( content_base - 1 );
        var base_unit = ( content_space - gap_space ) / content_base;
        var base_unit_plus_gap = base_unit + gap;

        return base_unit_plus_gap;
    }

    /**
     * Calculates `distance` (relative to this container) and `length` values of all kids, based on
     * the `layout_size` parameter value, layout algorithm type and kids boxes given in `kids_boxes`
     * parameter.
     *
     * If the `length` parameter of the `layout_size` is a valid number, the kids will be laid out into that space.
     *
     * If the `length` parameter of the `layout_size` is `UNDEFINED`, the kids will be laid out without
     * size constraint, and the `length` parameter of the result's `CalculatedLayout.layout_size` will be
     * set to however much space kids needed.
     *
     * @param layout_size available space `length` for layout, if `UNDEFINED` there will be no dimension constraint
     * @param kids_boxes `LayoutDimension`-s for all kids, **must** have values for all properties
     * @return `LayoutConfiguration` containing resulting layout `length` size and laid out kid dimension values
     */
    public function calculateLayout( layout_size : LayoutDimension, kids_boxes : Array<LayoutDimension>, options : LayoutOptions ) : LayoutOutputConfiguration {

        var non_constrained_layout_size =  LayoutDimension.fromLength( options.marginStart ).assignLengthIfRightIsSet( layout_size );
        var new_kids_boxes : Array<LayoutDimension> = [];

        switch( options.type ) {

            case None:
                for( kb in kids_boxes ) {
                    var new_kb : LayoutDimension = {
                        distance: kb.distance + options.marginStart,
                        length: kb.length,
                    }
                    new_kids_boxes.push( new_kb );
                    non_constrained_layout_size.expandTo( new_kb );
                }

            case Aligned:
                var k = switch( options.align ) {
                    case Start: 0.0;
                    case Center: 0.5;
                    case End: 1.0;
                    case None: throw new Exception("'align' parameter can't be 'None' for 'Aligned' type.");
                }
                var layout_length = 0.0;
                if( layout_size.length.isNumber() ) {
                    layout_length = layout_size.length - ( options.marginStart + options.marginEnd );
                } else {
                    for( kb in kids_boxes ) layout_length = Math.max( layout_length, kb.length );
                }
                for( kb in kids_boxes ) {
                    var new_kb : LayoutDimension = {
                        distance: k * ( layout_length - kb.length ) + options.marginStart,
                        length: kb.length,
                    }
                    new_kids_boxes.push( new_kb );
                    non_constrained_layout_size.expandTo( new_kb );
                }

            case Stacked:
                var k = switch( options.align ) {
                    case Start: 0.0;
                    case Center: 0.5;
                    case End: 1.0;
                    case None: throw new Exception("'align' parameter can't be 'None' for 'Stacked' type.");
                }
                var delta = 0.0;
                if( layout_size.length.isNumber() ) {
                    var kids_length = 0.0;
                    for( kb in kids_boxes ) kids_length += kb.length + options.gap;
                    kids_length -= options.gap;

                    delta = k * ( layout_size.length - ( options.marginStart + options.marginEnd ) - kids_length );
                }
                var currentDistance = options.marginStart;
                for( kb in kids_boxes ) {
                    var new_kb : LayoutDimension = {
                        distance: currentDistance + delta,
                        length: kb.length,
                    }
                    new_kids_boxes.push( new_kb );
                    non_constrained_layout_size.expandTo( new_kb );
                    currentDistance += new_kb.length + options.gap;
                }
        }

        non_constrained_layout_size.length += options.marginEnd;

        return {
            options: options,
            layout_size: layout_size.clone().assignLengthIfLeftNotSet( non_constrained_layout_size ),
            kids_dimensions: new_kids_boxes,
        };
    }
}
