// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.algorithm;

// using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


final class DimensionTools {
    private function new() {}

    public static function clone( dimension : LayoutDimension ) : LayoutDimension {
        return {
            length: dimension.length,
            distance: dimension.distance,
        }
    }

    public static function assignBox( l : LayoutDimension, r : LayoutDimension ) : LayoutDimension {
        l.distance = r.distance;
        l.length = r.length;

        return l;
    }

    public static function expandTo( size : Float, boxToAdd : LayoutDimension ) : Float {
        var length = boxToAdd.distance.isNumber() ? boxToAdd.distance : 0.0;
        length += boxToAdd.length.isNumber() ? boxToAdd.length : 0.0;

        var resultLength = size;
        // account for when size is not set
        if( ! resultLength.isNumber() ) resultLength = length;

        // if( resultBox.length.isNumber() && length > 0.0 ) {
        if( length > 0.0 ) {
            if( resultLength < length ) resultLength = length;
        }

        return resultLength;
    }

}
