// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.algorithm;

import rxlayout.types.SizeBox;
import rxlayout.types.LayoutOptions;
import rxlayout.types.SizeValue;
import rxlayout.types.Constants;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


typedef LayoutDimensionSizeType = {
    var distance: SizeValue;
    var length: SizeValue;
}


@:forward(distance, y, length, height)
abstract LayoutDimensionSize(LayoutDimensionSizeType) from LayoutDimensionSizeType to LayoutDimensionSizeType {

    public static var ZERO( get, never ) : LayoutDimensionSize;
    public static var UNDEFINED( get, never ) : LayoutDimensionSize;

    public inline function new() {
        this = UNDEFINED;
    }

    @:from
    static public function fromLayoutDimensionSizeType( a : LayoutDimensionSizeType ) : LayoutDimensionSize {
        return fromValues( a.distance, a.length );
    }
    @:to
    public function toLayoutDimensionSizeType() : LayoutDimensionSizeType {
        return { distance: this.distance, length: this.length };
    }

    static public function fromSizeBox( a : SizeBox, direction : LayoutDirection ) : LayoutDimensionSize {
        return switch( direction ) {
            case Row: fromValues( a.x, a.width );
            case Column:  fromValues( a.y, a.height );
        }
    }

    // @:from
    // static public function fromLayoutBox( a : LayoutBox ) : SizeBox {
    //     final out = new SizeBox();
    //     out.distance = a.distance.isNumber() ? a.distance.px() : SizeValue.UNDEFINED;
    //     out.y = a.y.isNumber() ? a.y.px() : SizeValue.UNDEFINED;
    //     out.length = a.length.isNumber() ? a.length.px() : SizeValue.UNDEFINED;
    //     out.height = a.height.isNumber() ? a.height.px() : SizeValue.UNDEFINED;
    //     return out;
    // }


    public static function fromValues( distance : SizeValue, length : SizeValue ) : LayoutDimensionSize {
        final out = new LayoutDimensionSize();
        out.distance = distance.clone();
        out.length = length.clone();
        return out;
    }
    public static function fromSize( length : Float, h : Float ) : LayoutDimensionSize {
        final out = new LayoutDimensionSize();
        out.distance = SizeValue.UNDEFINED;
        out.length = length.px();
        return out;
    }



    public inline function clone() : LayoutDimensionSize {
        return {
            distance: this.distance,
            length: this.length,
        }
    }

    public inline function assign( r : LayoutDimensionSize ) : Void {
        this.distance = r.distance;
        this.length = r.length;
    }

    /**
        Assign `r` length/height to `this` only if `this` is not set already.
    **/
    public function assignLengthIfLeftIsUndefined( r : LayoutDimensionSize ) : LayoutDimensionSize {

        if( this.length.isUndefined() ) this.length.assign( r.length );

        return this;
    }

    /**
        Assign `r` distance/y/length/height to `this` only if `this` is not set already.
    **/
    public function assignIfLeftIsUndefined( r : LayoutDimensionSize ) : LayoutDimensionSize {

        if( this.distance.isUndefined() ) this.distance.assign( r.distance );

        return assignLengthIfLeftIsUndefined( r );
    }

    /**
        Assign `r` length/height to `this` only if `r` contains valid values.
    **/
    public function assignLengthIfRightIsSet( r : LayoutDimensionSize ) : LayoutDimensionSize {

        if( ! r.length.isUndefined() ) this.length.assign( r.length );

        return this;
    }

    /**
        Assign `r` distance/y/length/height to `this` only if `r` contains valid values.
    **/
    public function assignIfRightIsSet( r : LayoutDimensionSize ) : LayoutDimensionSize {

        if( ! r.distance.isUndefined() ) this.distance.assign( r.distance );

        return assignLengthIfRightIsSet( r );
    }



    private inline static function get_ZERO() : LayoutDimensionSize {
        final v:LayoutDimensionSizeType = {
            distance: 0.px(),
            length: 0.px(),
        };
		return v;
	}
	private inline static function get_UNDEFINED() : LayoutDimensionSize {
        final v:LayoutDimensionSizeType = {
            distance: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            },
            length: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            },
        };
		return v;
	}
}
