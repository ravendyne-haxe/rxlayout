// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.algorithm;

import rxlayout.types.LayoutOptions.LayoutDirection;
import rxlayout.types.LayoutBox;
import rxlayout.types.Constants;

using rxlayout.RxLayoutUtil;


typedef LayoutDimensionType = {
    var distance: Float;
    var length: Float;
}

@:forward(distance, length)
abstract LayoutDimension(LayoutDimensionType) from LayoutDimensionType to LayoutDimensionType {

    public static var ZERO( get, never ) : LayoutDimension;
    public static var UNDEFINED( get, never ) : LayoutDimension;

    public inline function new() {
        this = UNDEFINED;
    }

    static public function fromLayoutBox( a : LayoutBox, direction : LayoutDirection ) : LayoutDimension {
        return switch( direction ) {
            case Row: fromValues( a.x, a.width );
            case Column:  fromValues( a.y, a.height );
        }
    }

    public function toLayoutBox( direction : LayoutDirection ) : LayoutBox {
        return switch( direction ) {
            case Row: { x: this.distance, y: Const.UNDEFINED_VALUE, width: this.length, height: Const.UNDEFINED_VALUE };
            case Column:  { x: Const.UNDEFINED_VALUE, y: this.distance, width: Const.UNDEFINED_VALUE, height: this.length };
        }
    }


    public static function fromValues( distance : Float, length : Float ) : LayoutDimension {
        final out = new LayoutDimension();
        out.distance = distance;
        out.length = length;
        return out;
    }
    public static function fromLength( length : Float ) : LayoutDimension {
        final out = new LayoutDimension();
        out.distance = Const.UNDEFINED_VALUE;
        out.length = length;
        return out;
    }



    public inline function clone() : LayoutDimension {
        return {
            distance: this.distance,
            length: this.length,
        }
    }

    public inline function assign( r : LayoutDimension ) : LayoutDimension {
        this.distance = r.distance;
        this.length = r.length;

        return this;
    }

    public function expandTo( boxToAdd : LayoutDimension ) : LayoutDimension {
        var length = boxToAdd.distance.isNumber() ? boxToAdd.distance : 0.0;
        length += boxToAdd.length.isNumber() ? boxToAdd.length : 0.0;

        // this will pick up distance,y of this layout box
        var resultBox = clone();
        // account for when sizeBox doesn't have w/h set
        resultBox.assignLengthIfLeftNotSet( fromLength( length ) );

        if( resultBox.length.isNumber() && length > 0.0 ) {
            if( resultBox.length < length ) resultBox.length = length;
        }

        // assigning box will not change distance,y because resultBox is
        // initially a clone() of this layout box
        return assign( resultBox );
    }

    /**
        Assign `r` distance,y,w,h to `this` only if `this` is not set already.
    **/
    public function assignIfLeftNotSet( r : LayoutDimension ) : LayoutDimension {
        // if( ! this.distance.isNumber() && r.distance.isNumber() ) this.distance = r.distance;
        // if( ! this.length.isNumber() && r.length.isNumber() ) this.length = r.length;
        if( ! this.distance.isNumber() ) this.distance = r.distance;
        if( ! this.length.isNumber() ) this.length = r.length;

        return this;
    }

    /**
        Assign `r` length/height to `this` only if `this` is not set already.
    **/
    public function assignLengthIfLeftNotSet( r : LayoutDimension ) : LayoutDimension {
        // if( ! this.length.isNumber() && r.length.isNumber() ) this.length = r.length;
        if( ! this.length.isNumber() ) this.length = r.length;

        return this;
    }

    /**
        Assign `r` length/height to `this` only if `r` contains valid values.
    **/
    public function assignLengthIfRightIsSet( r : LayoutDimension ) : LayoutDimension {
        if( r.length.isNumber() ) this.length = r.length;

        return this;
    }

    /**
        Assign `r` distance/y/length/height to `this` only if `r` contains valid values.
    **/
    public function assignIfRightIsSet( r : LayoutDimension ) : LayoutDimension {
        if( r.distance.isNumber() ) this.distance = r.distance;
        if( r.length.isNumber() ) this.length = r.length;

        return this;
    }

    public function stretchToMinSize( minDimension : LayoutDimension ) : LayoutDimension {
        if( minDimension.length.isNumber() ) {
            if( this.length.isNumber() ) {
                if( this.length < minDimension.length )
                    this.length = minDimension.length;
            } else {
                // min size sets the length if it is not specified
                this.length = minDimension.length;
            }
        }

        return this;
    }

    public function clampToMaxSize( maxDimension : LayoutDimension ) : LayoutDimension {
        if( maxDimension.length.isNumber() && this.length.isNumber() ) {
            if( this.length > maxDimension.length )
                this.length = maxDimension.length;
        }

        return this;
    }



    /**
     * If parameter dimension `length` is **defined**, this will
     * set this value's `length` to:
     *  - if parameter type is `PX`: to the parameter's `length` value
     *  - if parameter type is `PERCENT`: a percentage of this value's `length`, where percentage is given by the parameter's `length` value
     *  - if parameter type is `BASE`: this value's `length` multiplied by the parameter value's `length`
     *  - if parameter type is `AUTO`: this value's `length`
     *  - if parameter type is `UNDEFINED`: this value's `length` will remain unchanged
     *
     * @param a - `LayoutDimensionSize` to assign dimension values from
     * @return this `LayoutDimension` object
     */
    public function assignLengthFromLayoutDimensionSize( a : LayoutDimensionSize ) : LayoutDimension {

        if( ! a.length.isUndefined() ) {
            this.length = a.length.getPxValueOf( this.length );
        }

        return this;
    }

    /**
     * If a parameter dimension (`distance` or `length`) is **defined**, this will
     * set this value dimension to:
     *  - if parameter type is `PX`: to the parameter's value
     *  - if parameter type is `PERCENT`: a percentage of this dimension value, where percentage is given by the parameter's dimension
     *  - if parameter type is `BASE`: this dimension value multiplied by the parameter's dimension value
     *  - if parameter type is `AUTO`: this dimension value
     *  - if parameter type is `UNDEFINED`: this dimension value will remain unchanged
     *
     * @param a - `LayoutDimensionSize` to assign dimension values from
     * @return this `LayoutDimension` object
     */
    public function assignFromLayoutDimensionSize( a : LayoutDimensionSize ) : LayoutDimension {

        if( ! a.distance.isUndefined() ) {
            this.distance = a.distance.getPxValueOf( this.distance );
        }

        return assignLengthFromLayoutDimensionSize( a );
    }



    private inline static function get_ZERO() : LayoutDimension {
        final v:LayoutDimensionType = {distance:0, length:0};
		return v;
	}
	private inline static function get_UNDEFINED() : LayoutDimension {
        final v:LayoutDimensionType = {distance:Const.UNDEFINED_VALUE, length:Const.UNDEFINED_VALUE};
		return v;
	}
}
