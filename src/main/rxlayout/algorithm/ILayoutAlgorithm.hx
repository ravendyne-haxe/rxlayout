// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.algorithm;

import rxlayout.types.LayoutOptions;


typedef LayoutInputConfiguration = {
    options: LayoutOptions,
    layout_size: LayoutDimension,
    packed_kids_length: Float,
    preferred_size_of_kids: Array<LayoutDimensionSize>,
}

typedef LayoutOutputConfiguration = {
    options: LayoutOptions,
    layout_size: LayoutDimension,
    kids_dimensions: Array<LayoutDimension>,
}

typedef CalculatedLayoutResult = {
    mainAxisResult: LayoutOutputConfiguration,
    crossAxisResult: LayoutOutputConfiguration,
}

interface ILayoutAlgorithm {
    function calculate( mainAxisConfiguration: LayoutInputConfiguration, crossAxisConfiguration: LayoutInputConfiguration ) : CalculatedLayoutResult;
    function calculateAvailableLayoutSize( concrete_layout_size : LayoutDimension, options : LayoutOptions ) : LayoutDimension;
    function calculateKidsLayoutDimension( inputConfiguration: LayoutInputConfiguration ) : Array<LayoutDimension>;
    function calculateLayout( layout_size : LayoutDimension, kids_boxes : Array<LayoutDimension>, options : LayoutOptions ) : LayoutOutputConfiguration;
}
