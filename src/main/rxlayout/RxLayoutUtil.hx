// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout;

import rxlayout.types.SizeValue;


final class RxLayoutUtil {
    private function new() {}

    public static inline function isNumber( v : Float ) : Bool {
        return v != null && Math.isFinite( v );
    }

    public static inline function nanIfNotNumber( v : Float ) : Float {
        return valueIfNotNumber( v, Math.NaN );
    }

    public static inline function valueIfNotNumber( v : Float, dv : Float ) : Float {
        return isNumber( v ) ? v : dv;
    }

    /**
        Returns, in order of priority:
            - if `sv` is of type `PX`: `sv` parameter value
            - if `sv` is of type `PERCENT` **and** `v` `isNumber()`: percentage of `v` parameter
            - if `sv` is of type `BASE` **and** `v` `isNumber()`: `v` multiplied by `sv`, where `sv` coerced to `int` >= `0`
            - if `sv` is of type `TAUTO` **and** `v` `isNumber()`: `v`
            - if `sv` is of type `TUNDEFINED` **or** `v` **not** `isNumer()`: `v`
    **/
    public static inline function getPxValueOf( sv : SizeValue, v : Float ) : Float {
        var r = v;
        if( sv.type == PX ) {
            r = sv.value;
        } else if( sv.type == PERCENT && isNumber(v) ) {
            r = v * sv.value / 100.0;
        } else if( sv.type == BASE && isNumber(v) ) {
            r = v * getValidBaseValue( sv.value );
        } else if( sv.type == TAUTO && isNumber(v) ) {
            r = v;
        }
        return r;
    }

    /**
     * A valid `BASE` value is an integer number greater than `0`.
     *
     * This will return a value that is rounded to non-fractional part of
     * the `Float` parameter, or `0.0` if the parameter is less than `0.0`.
     *
     * If the parameter is not a valid floating point number, `0.0` is returned.
     *
     * @param v
     * @return `Float` type integer value greater than or equal to `0.0`
     */
    public static inline function getValidBaseValue( v : Float ) : Float {
        return isNumber( v ) ? Math.max( 0.0, Math.ffloor( v ) ) : 0.0;
    }
}
