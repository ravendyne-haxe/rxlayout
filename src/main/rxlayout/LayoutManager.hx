// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout;

import rxlayout.algorithm.DefaultLayoutAlgorithm;
import rxlayout.algorithm.ILayoutAlgorithm;

final class LayoutManager {
    private function new() {}

    static var INSTANCE : ILayoutAlgorithm = null;

    public static function getLayoutAlgorithm() : ILayoutAlgorithm {
        if( INSTANCE == null ) {
            INSTANCE = new DefaultLayoutAlgorithm();
        }

        return INSTANCE;
    }

    public static function setLayoutAlgorithm( algorithm : ILayoutAlgorithm ) {
        if( algorithm == null ) return;
        INSTANCE = algorithm;
    }
}
