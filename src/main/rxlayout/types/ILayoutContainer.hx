// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.LayoutBox;


interface ILayoutContainer {

    var view_element(get,set) : IViewElement;

    /**
        This container's size as set by an outside code (i.e. **not** a code that's part of
        an implementation of this interface). This property **must never** be set by
        any of the code executed as a result of `doLayout()` method call. It **should not** be
        set/changed by any `ILayoutContainer` implementations, except in special cases (i.e. in constructor).
    */
    var assigned_box(get,never) : SizeBox;
    function setPositionAndSizeBox( box : SizeBox ) : Void;
    function setPositionAndSize( x : SizeValue, y : SizeValue, width : SizeValue, height : SizeValue ) : Void;
    function setPosition( x : SizeValue, y : SizeValue ) : Void;
    function setSize( width : SizeValue, height : SizeValue ) : Void;
    /**
        Returns this container's `assigned_box` box if set, or its `packed_size` otherwise.
        `x`, `y`, `width` and `height` properties of the returned `SizeBox` **must** have value of type other than of `SizeBox.UNDEFIND`.
    */
    function getPreferredBox() : SizeBox;

    /**
        This container's size after it applies its own layout algorithm on its children:
        - without having its own size set, and
        - without any size bounds imposed by parent container.
        `width` and `height` properties of packed_size **must** always be a valid floating point number.
        `x` and `y` properties of packed_size **must** always be `0`.
    */
    var packed_size(default,null) : LayoutBox;
    function updatePackedSize() : Void;

    /**
        This container's box after latest call to its `doLayout()` method.
        `x`, `y`, `width` and `height` dimension properties of this box **must**
        always be a valid floating point number.
    */
    var layout_box(default,null) : LayoutBox;

    function doLayout( your_advised_layout_box : LayoutBox ) : LayoutBox;

    /**
        This **should** contain a reference to this container's parent container.
    **/
    var parent(get,set) : ILayoutContainer;

    var children(get,never) : Iterator<ILayoutContainer>;

    /**
        Adds a list of containers as children of this container, for purposes of applying this container's layout algorithm to them.

        If a container from the list is already a child of this container,
        it **must** be skipped and **not** added as a child again.

        A call to this method **must** update this container's `packed_size` box.
    */
    function addChildren( children : Array<ILayoutContainer> ) : Void;

    /**
        Removas a list of containers from the list of children of this container, for purposes of applying this container's layout algorithm to them.

        If a container from the list is not a child of this container, no action is taken on that container.

        A call to this method **must** update this container's `packed_size` box.
    */
    function removeChildren( children : Array<ILayoutContainer> ) : Void;
}
