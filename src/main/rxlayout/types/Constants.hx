// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

final class Const {
    private function new() {}
    public static var UNDEFINED_VALUE( default, never ) : Float = Math.NaN;
}
