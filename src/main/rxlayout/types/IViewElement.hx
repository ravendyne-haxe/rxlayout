// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.LayoutBox;

interface IViewElement {
    function setBox( box : LayoutBox ) : Void;
    // function getBox() : LayoutBox;
}
