// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.Constants;
import rxlayout.types.SizeBox;

using rxlayout.RxLayoutUtil;


typedef LayoutBoxType = {
    var ?x : Float;
    var ?y : Float;
    var width : Float;
    var height : Float;
}

@:forward(x, y, width, height)
abstract LayoutBox(LayoutBoxType) from LayoutBoxType to LayoutBoxType {

    public static var ZERO( get, never ) : LayoutBox;
    public static var UNDEFINED( get, never ) : LayoutBox;

    public inline function new() {
        this = UNDEFINED;
    }

    @:from
    static public function fromLayoutBoxType( a : LayoutBoxType ) : LayoutBox {
        return fromValues( a.x, a.y, a.width, a.height );
    }
    @:to
    public function toLayoutBoxType() : LayoutBoxType {
        return { x: this.x, y: this.y, width: this.width, height: this.height };
    }

    @:from
    static public function fromSizeBox( a : SizeBox ) : LayoutBox {
        return ZERO.assignBoxFromSizeBox( a );
    }


    public static function fromValues( x : Float, y : Float, w : Float, h : Float ) : LayoutBox {
        final out = new LayoutBox();
        out.x = x;
        out.y = y;
        out.width = w;
        out.height = h;
        return out;
    }
    public static function fromSize( w : Float, h : Float ) : LayoutBox {
        final out = new LayoutBox();
        out.x = Const.UNDEFINED_VALUE;
        out.y = Const.UNDEFINED_VALUE;
        out.width = w;
        out.height = h;
        return out;
    }



    public inline function clone() : LayoutBox {
        return {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
        }
    }

    public inline function assignBox( r : LayoutBox ) : LayoutBox {
        this.x = r.x;
        this.y = r.y;
        this.width = r.width;
        this.height = r.height;

        return this;
    }

    public function expandTo( boxToAdd : LayoutBox ) : LayoutBox {
        var width = boxToAdd.x.isNumber() ? boxToAdd.x : 0.0;
        var height = boxToAdd.y.isNumber() ? boxToAdd.y : 0.0;
        width += boxToAdd.width.isNumber() ? boxToAdd.width : 0.0;
        height += boxToAdd.height.isNumber() ? boxToAdd.height : 0.0;

        // this will pick up x,y of this layout box
        var resultBox = clone();
        // account for when sizeBox doesn't have w/h set
        resultBox.assignSizeIfLeftNotSet( fromSize( width, height ) );

        if( resultBox.width.isNumber() && width > 0.0 ) {
            if( resultBox.width < width ) resultBox.width = width;
        }
        if( resultBox.height.isNumber() && height > 0.0 ) {
            if( resultBox.height < height ) resultBox.height = height;
        }

        // assigning box will not change x,y because resultBox is
        // initially a clone() of this layout box
        return assignBox( resultBox );
    }

    /**
        Assign `r` x,y,w,h to `this` only if `this` is not set already.
    **/
    public function assignBoxIfLeftNotSet( r : LayoutBox ) : LayoutBox {
        // if( ! this.x.isNumber() && r.x.isNumber() ) this.x = r.x;
        // if( ! this.y.isNumber() && r.y.isNumber() ) this.y = r.y;
        // if( ! this.width.isNumber() && r.width.isNumber() ) this.width = r.width;
        // if( ! this.height.isNumber() && r.height.isNumber() ) this.height = r.height;
        if( ! this.x.isNumber() ) this.x = r.x;
        if( ! this.y.isNumber() ) this.y = r.y;
        if( ! this.width.isNumber() ) this.width = r.width;
        if( ! this.height.isNumber() ) this.height = r.height;

        return this;
    }

    /**
        Assign `r` width/height to `this` only if `this` is not set already.
    **/
    public function assignSizeIfLeftNotSet( r : LayoutBox ) : LayoutBox {
        // if( ! this.width.isNumber() && r.width.isNumber() ) this.width = r.width;
        // if( ! this.height.isNumber() && r.height.isNumber() ) this.height = r.height;
        if( ! this.width.isNumber() ) this.width = r.width;
        if( ! this.height.isNumber() ) this.height = r.height;

        return this;
    }

    /**
        Assign `r` width/height to `this` only if `r` contains valid values.
    **/
    public function assignSizeIfRightIsSet( r : LayoutBox ) : LayoutBox {
        if( r.width.isNumber() ) this.width = r.width;
        if( r.height.isNumber() ) this.height = r.height;

        return this;
    }

    /**
        Assign `r` x/y/width/height to `this` only if `r` contains valid values.
    **/
    public function assignBoxIfRightIsSet( r : LayoutBox ) : LayoutBox {
        if( r.x.isNumber() ) this.x = r.x;
        if( r.y.isNumber() ) this.y = r.y;
        if( r.width.isNumber() ) this.width = r.width;
        if( r.height.isNumber() ) this.height = r.height;

        return this;
    }

    public function stretchToMinSize( minBox : LayoutBox ) : LayoutBox {
        if( minBox.width.isNumber() ) {
            if( this.width.isNumber() ) {
                if( this.width < minBox.width )
                    this.width = minBox.width;
            } else {
                // min size sets the width if it is not specified
                this.width = minBox.width;
            }
        }
        if( minBox.height.isNumber() ) {
            if( this.height.isNumber() ) {
                if( this.height < minBox.height )
                    this.height = minBox.height;
            } else {
                // min size sets the height if it is not specified
                this.height = minBox.height;
            }
        }

        return this;
    }

    public function clampToMaxSize( maxBox : LayoutBox ) : LayoutBox {
        if( maxBox.width.isNumber() && this.width.isNumber() ) {
            if( this.width > maxBox.width )
                this.width = maxBox.width;
        }
        if( maxBox.height.isNumber() && this.height.isNumber() ) {
            if( this.height > maxBox.height )
                this.height = maxBox.height;
        }

        return this;
    }



    /**
     * If a parameter size dimension (`width` or `height`) is **defined**, this will
     * set this value's size dimension to:
     *  - if parameter type is `PX`: to the parameter's value
     *  - if parameter type is `PERCENT`: a percentage of this value, where percentage is given by the parameter
     *  - if parameter type is `BASE`: this value multiplied by the parameter value
     *  - if parameter type is `AUTO`: this value will remain unchanged
     *  - if parameter type is `UNDEFINED`: this value will remain unchanged
     *
     * @param a - `SizeBox` to assign dimension values from
     * @return this `LayoutBox` object
     */
    public function assignSizeFromSizeBox( a : SizeBox ) : LayoutBox {

        if( ! a.width.isUndefined() ) {
            this.width = a.width.getPxValueOf( this.width );
        }
        if( ! a.height.isUndefined() ) {
            this.height = a.height.getPxValueOf( this.height );
        }

        return this;
    }

    /**
     * If a parameter dimension (`x`, `y`, `width` or `height`) is **defined**, this will
     * set this value's dimension to:
     *  - if parameter type is `PX`: to the parameter's value
     *  - if parameter type is `PERCENT`: a percentage of this value, where percentage is given by the parameter
     *  - if parameter type is `BASE`: this value multiplied by the parameter value
     *  - if parameter type is `AUTO`: this value will remain unchanged
     *  - if parameter type is `UNDEFINED`: this value will remain unchanged
     *
     * @param a - `SizeBox` to assign dimension values from
     * @return this `LayoutBox` object
     */
    public function assignBoxFromSizeBox( a : SizeBox ) : LayoutBox {

        if( ! a.x.isUndefined() ) {
            this.x = a.x.getPxValueOf( this.x );
        }
        if( ! a.y.isUndefined() ) {
            this.y = a.y.getPxValueOf( this.y );
        }

        return assignSizeFromSizeBox( a );
    }



    private inline static function get_ZERO() : LayoutBox {
        final v:LayoutBoxType = {x:0, y:0, width:0, height:0};
		return v;
	}
	private inline static function get_UNDEFINED() : LayoutBox {
        final v:LayoutBoxType = {x:Const.UNDEFINED_VALUE, y:Const.UNDEFINED_VALUE, width:Const.UNDEFINED_VALUE, height:Const.UNDEFINED_VALUE};
		return v;
	}
}
