// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.SizeValue;
import rxlayout.types.Constants;

using rxlayout.types.SizeBoxTools;
using rxlayout.RxLayoutUtil;


typedef SizeBoxType = {
    var x: SizeValue;
    var y: SizeValue;
    var width: SizeValue;
    var height: SizeValue;
}


@:forward(x, y, width, height)
abstract SizeBox(SizeBoxType) from SizeBoxType to SizeBoxType {

    public static var ZERO( get, never ) : SizeBox;
    public static var UNDEFINED( get, never ) : SizeBox;

    public inline function new() {
        this = UNDEFINED;
    }

    @:from
    static public function fromSizeBoxType( a : SizeBoxType ) : SizeBox {
        return fromValues( a.x, a.y, a.width, a.height );
    }
    @:to
    public function toSizeBoxType() : SizeBoxType {
        return { x: this.x, y: this.y, width: this.width, height: this.height };
    }

    @:from
    static public function fromLayoutBox( a : LayoutBox ) : SizeBox {
        final out = new SizeBox();
        out.x = a.x.isNumber() ? a.x.px() : SizeValue.UNDEFINED;
        out.y = a.y.isNumber() ? a.y.px() : SizeValue.UNDEFINED;
        out.width = a.width.isNumber() ? a.width.px() : SizeValue.UNDEFINED;
        out.height = a.height.isNumber() ? a.height.px() : SizeValue.UNDEFINED;
        return out;
    }


    public static function fromValues( x : SizeValue, y : SizeValue, w : SizeValue, h : SizeValue ) : SizeBox {
        final out = new SizeBox();
        out.x = x.clone();
        out.y = y.clone();
        out.width = w.clone();
        out.height = h.clone();
        return out;
    }
    public static function fromSize( w : Float, h : Float ) : SizeBox {
        final out = new SizeBox();
        out.x = SizeValue.UNDEFINED;
        out.y = SizeValue.UNDEFINED;
        out.width = w.isNumber() ? w.px() : SizeValue.UNDEFINED;
        out.height = h.isNumber() ? h.px() : SizeValue.UNDEFINED;
        return out;
    }



    public inline function clone() : SizeBox {
        return {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
        }
    }

    public inline function assignBox( r : SizeBox ) : Void {
        this.x = r.x;
        this.y = r.y;
        this.width = r.width;
        this.height = r.height;
    }

    /**
        Assign `r` width/height to `this` only if `this` is not set already.
    **/
    public function assignSizeIfLeftIsUndefined( r : SizeBox ) : SizeBox {

        if( this.width.isUndefined() ) this.width.assign( r.width );
        if( this.height.isUndefined() ) this.height.assign( r.height );

        return this;
    }

    /**
        Assign `r` x/y/width/height to `this` only if `this` is not set already.
    **/
    public function assignBoxIfLeftIsUndefined( r : SizeBox ) : SizeBox {

        if( this.x.isUndefined() ) this.x.assign( r.x );
        if( this.y.isUndefined() ) this.y.assign( r.y );

        return assignSizeIfLeftIsUndefined( r );
    }

    /**
        Assign `r` width/height to `this` only if `r` contains valid values.
    **/
    public function assignSizeIfRightIsSet( r : SizeBox ) : SizeBox {

        if( ! r.width.isUndefined() ) this.width.assign( r.width );
        if( ! r.height.isUndefined() ) this.height.assign( r.height );

        return this;
    }

    /**
        Assign `r` x/y/width/height to `this` only if `r` contains valid values.
    **/
    public function assignBoxIfRightIsSet( r : SizeBox ) : SizeBox {

        if( ! r.x.isUndefined() ) this.x.assign( r.x );
        if( ! r.y.isUndefined() ) this.y.assign( r.y );

        return assignSizeIfRightIsSet( r );
    }



    private inline static function get_ZERO() : SizeBox {
        final v:SizeBoxType = {
            x: 0.px(),
            y: 0.px(),
            width: 0.px(),
            height: 0.px()
        };
		return v;
	}
	private inline static function get_UNDEFINED() : SizeBox {
        final v:SizeBoxType = {
            x: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            },
            y: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            },
            width: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            },
            height: {
                type: TUNDEFINED,
                value: Const.UNDEFINED_VALUE
            }
        };
		return v;
	}
}
