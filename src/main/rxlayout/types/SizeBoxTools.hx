// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.SizeValue;


final class SizeBoxTools {
    private function new() {}
    public static function px( v: Float ) : SizeValue {
        return {type: PX, value: v};
    }
    public static function percent( v: Float ) : SizeValue {
        return {type: PERCENT, value: v};
    }
    public static function base( v: Float ) : SizeValue {
        return {type: BASE, value: v};
    }
}
