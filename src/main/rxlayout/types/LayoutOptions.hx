// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;


enum LayoutDirection {
    Row; // main axis - x; cross axis - y;
    Column; // main axis - y; cross axis - x;
}

enum LayoutType {
    Aligned;
    Stacked;
    None;
}

enum AxisAlignment {
    Start; // default
    Center;
    End;
    // SpaceAround;
    // SpaceBetween;
    // SpaceEvenly;
    None;
}


typedef LayoutOptions = {
    var type : LayoutType;

    var align : AxisAlignment;

    var marginStart : Float;
    var marginEnd : Float;

    var gap : Float;
    var base : Float;
}





enum AlignMainAxis {
    Start; // default
    Center;
    End;
    // SpaceAround;
    // SpaceBetween;
    // SpaceEvenly;
    None;
}

enum AlignCrossAxis {
    Start; // default
    Center;
    End;
    Stretch;
    None;
}
