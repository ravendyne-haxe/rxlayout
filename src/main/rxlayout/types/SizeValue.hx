// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxlayout.types;

import rxlayout.types.Constants;

using rxlayout.RxLayoutUtil;


enum SizeValueTypeType {
    PX;
    PERCENT;
    BASE;
    TAUTO;
    TUNDEFINED;
}

typedef SizeValueType = {
    var type: SizeValueTypeType;
    var value: Float;
}


@:forward(type,value)
abstract SizeValue(SizeValueType) from SizeValueType to SizeValueType {

    public static var ZERO( get, never ) : SizeValue;
    public static var AUTO( get, never ) : SizeValue;
    public static var UNDEFINED( get, never ) : SizeValue;

    public inline function new() {
        this = UNDEFINED;
    }

    @:from
    static public function fromSizeValueType( a : SizeValueType ) : SizeValue {
        final out = new SizeValue();
        out.type = a.type;
        out.value = a.value;
        return out;
    }
    @:to
    public function toSizeValueType() : SizeValueType {
        return { type: this.type, value: this.value };
    }



    public inline function clone() : SizeValue {
        return {
            type: this.type,
            value: this.value
        }
    }

    public inline function assign( r : SizeValue ) : SizeValue {
        this.type = r.type;
        this.value = r.value;

        return this;
    }

    public inline function isUndefined() : Bool {
        return this.type == TUNDEFINED || ( this.type != TAUTO && ! this.value.isNumber() );
    }

    public inline function isAuto() : Bool {
        return this.type == TAUTO;
    }



    private inline static function get_ZERO() : SizeValue {
        final v:SizeValue = {
            type: PX,
            value: 0.0
        };
        return v;
    }
	private inline static function get_AUTO() : SizeValue {
        final v:SizeValue = {
            type: TAUTO,
            value: Const.UNDEFINED_VALUE
        };
		return v;
	}
	private inline static function get_UNDEFINED() : SizeValue {
        final v:SizeValue = {
            type: TUNDEFINED,
            value: Const.UNDEFINED_VALUE
        };
		return v;
	}
}
